// YOU MAY MODIFY THIS FILE, AS LONG AS YOU DON'T MODIFY THE API DEFINED IN THE .H FILE
#include <iostream>
#include <cstdio>
#include <string.h>
#include "Bitwise.h"

// DO NOT MODIFY THIS. THIS IS USED TO PRINT NUMBERS AS BINARY
const char *byte_to_binary(int x);


int main() {
    unsigned char a, b, ops, r;
    bool rb;

    // Q1 - combine:
    a = 0b00001001;
    b = 0b00000110;
    
    r = combine(a, b);
    
    printf("Q1: The result of combine is: %s\n", byte_to_binary(r));


    // Q2 - check_rule:
    a = 0b01001101;
    b = 0b10101101;

    rb = check_rule(a, b);

    printf("Q2: The result of check_rule is: %s\n", rb ? "true" : "false");


    // Q3 - compressed_sum:
    a = 0b11010110;

    r = compressed_sum(a);

    printf("Q3: The result of compressed_sum is: %s\n", byte_to_binary(r));


    // Q4 - binary_operations
    a = 0b00101101;
    b = 0b01001111;
    ops = 0b11001101;

    r = binary_operations(a, b, ops);

    printf("Q4: The result of binary_operations is: %s\n", byte_to_binary(r));
}



// DO NOT MODIFY THIS. THIS IS USED TO PRINT NUMBERS AS BINARY
const char *byte_to_binary(int x)
{
    static char b[9];
    b[0] = '\0';
    for (int z = 128; z > 0; z >>= 1){ strcat(b, ((x & z) == z) ? "1" : "0"); }
    return b;
}