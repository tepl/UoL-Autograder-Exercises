#include "Bitwise.h"
#include "json.hpp"
#include "cpp_eval_util.h"
#include <fstream>
#include <vector>
#include <string>
#include <iostream>
#include <exception>
#include <tuple>
#include <math.h>

using namespace std;

#define TEST_CASE_COUNT 8

class CombineEvaluator : public Evaluator<unsigned char>{
  public:
    unsigned char test_cases[TEST_CASE_COUNT][3] = {
        {0b00001001, 0b00000110, 0b10010110},
        {0b00001111, 0b00001111, 0b11111111},
        {0b00000000, 0b00000000, 0b00000000},
        {0b00001111, 0b00000000, 0b11110000},
        {0b00000000, 0b00001111, 0b00001111},
        {0b00001010, 0b00001010, 0b10101010},
        {0b00000101, 0b00001010, 0b01011010},
        {0b00001101, 0b00000110, 0b11010110}
    };

    CombineEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "combine(0b" + byte_to_binary(test_cases[i][0]) + ", 0b" + byte_to_binary(test_cases[i][1]) + ")";
    }

    unsigned char GetResult(int i){
      return combine(test_cases[i][0], test_cases[i][1]);
    }

    float GetScore(int i, unsigned char result){
      return result == test_cases[i][2] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, unsigned char result, float score){
      return "Returned 0b" + byte_to_binary(result) + ", expecting 0b" + byte_to_binary(test_cases[i][2]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  CombineEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
