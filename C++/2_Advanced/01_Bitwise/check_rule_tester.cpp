#include "Bitwise.h"
#include "json.hpp"
#include "cpp_eval_util.h"
#include <fstream>
#include <vector>
#include <string>
#include <iostream>
#include <exception>
#include <tuple>
#include <math.h>

using namespace std;

#define TEST_CASE_COUNT 16

class CheckRuleEvaluator : public Evaluator<bool>{
  public:

  unsigned char test_cases[TEST_CASE_COUNT][2] = {
        {0b01001101, 0b10101101},
        {0b10110111, 0b10011011},
        {0b10000001, 0b01010001},
        {0b10000001, 0b01010001},
        {0b01001101, 0b00011101},
        {0b01001111, 0b00011111},
        {0b00011011, 0b11001011},
        {0b10011101, 0b00101101},
        {0b01010110, 0b10000110},
        {0b10100110, 0b11011010},
        {0b01111011, 0b11001100},
        {0b00010010, 0b11101100},
        {0b11110111, 0b10000101},
        {0b10000010, 0b11100101},
        {0b01000011, 0b11001101},
        {0b10000010, 0b10101001}
  };

  bool expected_outputs[TEST_CASE_COUNT] = {
      true, false,
      true, true, true, true, true, true, true,
      false, false, false, false, false, false, false
  };

    CheckRuleEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "check_rule(0b" + byte_to_binary(test_cases[i][0]) + ", 0b" + byte_to_binary(test_cases[i][1]) + ")";
    }

    bool GetResult(int i){
      return check_rule(test_cases[i][0], test_cases[i][1]);
    }

    float GetScore(int i, bool result){
      return result == expected_outputs[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, bool result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(expected_outputs[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};


int main(int argc, char **argv) {
  CheckRuleEvaluator evaluator(argc, argv);
  return evaluator.Run();
}