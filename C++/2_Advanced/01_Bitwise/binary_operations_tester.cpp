#include "Bitwise.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 16

class BinaryOperationsEvaluator : public Evaluator<unsigned char>{
  public:
    BinaryOperationsEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    unsigned char test_cases[TEST_CASE_COUNT][4] = {
      {0b00101101, 0b01001111, 0b11001101, 0b00000101},
      {0b01100001, 0b01000101, 0b11111111, 0b00000001},
      {0b11101001, 0b00010011, 0b00101011, 0b00000000},
      {0b10110011, 0b01010100, 0b01100010, 0b00011011},
      {0b10100011, 0b01000101, 0b00001111, 0b11111111},
      {0b10110101, 0b01100100, 0b11000111, 0b00010001},
      {0b11101111, 0b00010110, 0b10110001, 0b11100100},
      {0b10101100, 0b00011111, 0b11011010, 0b00001100},
      {0b10111011, 0b11011010, 0b01111011, 0b00011111},
      {0b10111100, 0b10010101, 0b01100101, 0b00010101},
      {0b11111010, 0b00001011, 0b10000111, 0b11110000},
      {0b10011100, 0b00111110, 0b11001000, 0b00010100},
      {0b11110111, 0b00111010, 0b01111010, 0b00011111},
      {0b00000011, 0b01001000, 0b10111101, 0b00000000},
      {0b01110000, 0b10111110, 0b00101111, 0b11111100},
      {0b11000100, 0b01110100, 0b11000110, 0b00000000}
    };

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "binary_operations(0b" + byte_to_binary(test_cases[i][0]) + ", 0b" + byte_to_binary(test_cases[i][1]) + ", 0b" + byte_to_binary(test_cases[i][2]) + ")";
    }

    unsigned char GetResult(int i){
      return binary_operations(test_cases[i][0], test_cases[i][1], test_cases[i][2]);
    }

    float GetScore(int i, unsigned char result){
      return result == test_cases[i][3] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, unsigned char result, float score){
      return "Returned 0b" + byte_to_binary(result) + ", expecting 0b" + byte_to_binary(test_cases[i][3]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  BinaryOperationsEvaluator evaluator(argc, argv);
  return evaluator.Run();
}