#ifndef BITWISE_H
#define BITWISE_H

// DO NOT MODIFY THIS FILE!
// THIS FILE DEFINES THE STRUCTURE OF THE FUNCTIONS WHICH WILL BE TESTED
// IF YOU MODIFY THE STRUCUTRE OF THE FUNCTION, YOUR RESULTS WILL NOT BE VALID

// API: Both a and b are 8 bit binary numbers, where the 4 most significant bits are all 0 and the 4 least significant bits encode a number
// Return an 8 bit binary number, where the 4 most significant bits are the last 4 bits of a and the 4 least significant bits are the last 4 bits of b
// Params:
//  a - first binary number
//  b - second binary number
// Returns:
//  an 8 bit number (unsigned char) combination of a and b
// Example:
//  a - 0b00001001
//  b - 0b00000110
// return - 0b10010110
unsigned char combine(unsigned char a, unsigned char b);

// API: Validate if the two passed in binary numbers adhere to the following rule
// Rule: return true if bits 1-4 are all the same, and if bits 6-8 are all different. Otherwise return false
// (note: the nth bit in a binary number refers to the nth bit from the right. So in 00000111 bits 1-3 are 1)
// Params:
//  a - first binary number
//  b - second binary number
// Returns:
//  true if the two numbers adhere to the rule, false if they don't
// Example:
//  a - 0b01001101, b - 0b10101101, return: true
//  a - 0b10110111, b - 0b10011011, return: false
bool check_rule(unsigned char a, unsigned char b);

// API: Decompress the two numbers encoded in a and add them
// Every odd bit encodes one 4 bit number and every even bit encodes a second 4 bit number. Extract the two numbers and add them
// Params:
//  a - the binary number that has two numbers encoded in it.
// Returns:
//  the sum of the two numbers
// Example:
//  a - 0b11010110
// num1 - 0b00001110, num2 - 0b00001001
// return 0b00010111
unsigned char compressed_sum(unsigned char a);

// API: Perform a sequence of operations on a and b
// v is initially equals to a, and each operation in the sequence is executed on v
// operations are enabled or disabled by ops
// (note: the nth bit in a binary number refers to the nth bit from the right. So in 01000000 the 7th bit is 1)
// Params:
//  a - first binary number
//  b - second binary number
// ops - 
//  if the 1st bit is 1 bitwise AND v and b
//  if the 2nd bit is 1 bitwise OR v and b
//  if the 3rd bit is 1 invert v
//  if the 4th bit is 1 bitwise XOR v with b
//  if the 5th bit is 1 bitwise NAND v with b
//  if the 6th bit is 1 shift v to the left by 2
//  if the 7th bit is 1 shift v to the right by 3
//  if the 8th bit is 1 bitwise AND v with a
// Return:
// the final value of v
// Example:
//  a - 0b00101101
//  b - 0b01001111
//  ops - 0b11001101
// v = a = 0b00101101
// 1st bit is 1: v = 0b00001101
// 2nd bit is 0: v doesn't change
// 3rd bit is 1: v = 0b11110010
// 4th bit is 1: v = 0b10111101
// 5th bit is 0: v doesn't change
// 6th bit is 0: v doesn't change
// 7th bit is 1: v = 0b00010111
// 8th bit is 1: v = 0b00000101
// return 0b00000101
// Other examples:
// a - 0b01100001, b - 0b01000101, ops - 0b11111111, return - 0b00000001
// a - 0b11101001, b - 0b00010011, ops - 0b00101011, return - 0b00000000
// a - 0b10110011, b - 0b01010100, ops - 0b01100010, return - 0b00011011
unsigned char binary_operations(unsigned char a, unsigned char b, unsigned char ops);


#endif  // BITWISE_H