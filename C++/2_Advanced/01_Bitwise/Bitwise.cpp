#include "Bitwise.h"

// import any required libraries here

unsigned char combine(unsigned char a, unsigned char b){
    return a << 4 | (b & 0x0f);
}

bool check_rule(unsigned char a, unsigned char b){
    return ((a & b & 0xE0) | ((a ^ b) & 0x0f)) == 0x00;
}

unsigned char compressed_sum(unsigned char a){
    unsigned char num2 = a & 0xaa;
    unsigned char num1 = a & 0x55;
    num1 = ( num1       & 0x1) |
           ((num1 >> 1) & 0x2) |
           ((num1 >> 2) & 0x4) |
           ((num1 >> 3) & 0x8);
    num2 = ((num2 >> 1) & 0x1) |
           ((num2 >> 2) & 0x2) |
           ((num2 >> 3) & 0x4) |
           ((num2 >> 4) & 0x8);
    return num2 + num1;
}

unsigned char binary_operations(unsigned char a, unsigned char b, unsigned char ops){
    unsigned char v = a;
    if(ops & 0x01)
        v = v & b;
    if(ops & 0x02)
        v = v | b;
    if(ops & 0x04)
        v = ~v;
    if(ops & 0x08)
        v = v ^ b;
    if(ops & 0x10)
        v = ~(v & b);
    if(ops & 0x20)
        v = v << 2;
    if(ops & 0x40)
        v = v >> 3;
    if(ops & 0x80)
        v = v & a;
    return v;
}


