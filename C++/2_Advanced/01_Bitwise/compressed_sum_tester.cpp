#include "Bitwise.h"
#include "json.hpp"
#include "cpp_eval_util.h"
#include <fstream>
#include <vector>
#include <string>
#include <iostream>
#include <exception>
#include <tuple>
#include <math.h>

using namespace std;

#define TEST_CASE_COUNT 16


class CompressedSumEvaluator : public Evaluator<unsigned char>{
  public:
    unsigned char test_cases[TEST_CASE_COUNT][2] = {
        {0b11000111, 20},
        {0b11010101, 23},
        {0b11110011, 26},
        {0b11100000, 20},
        {0b00000011, 2},
        {0b11010110, 23},
        {0b01000001, 9},
        {0b00101010, 7},
        {0b11101001, 23},
        {0b10010101, 15},
        {0b11110110, 27},
        {0b00110100, 10},
        {0b01010001, 13},
        {0b10111100, 20},
        {0b11100001, 21},
        {0b01001111, 14}
  };

    CompressedSumEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "compressed_sum(0b" + byte_to_binary(test_cases[i][0]) + ")";
    }

    unsigned char GetResult(int i){
      return compressed_sum(test_cases[i][0]);
    }

    float GetScore(int i, unsigned char result){
      return result == test_cases[i][1] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, unsigned char result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][1]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  CompressedSumEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
