#include "ArrayExtremes.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

class MostFrequentNumberEvaluator : public Evaluator<int>{
  public:
    vector<vector<int>> inputs {
      { 5, 8, 7, 3, 8, 5, 2, 8 },
      { 11, 85, 8, 95, 85, 51, 31, 11 },
      { },
      { 1, 10 },
      { 1, 1, 1, 1, 1 },
      { -100, -100, -8, -3, -5, -20, -61 },
      { 3, 4, 3, 3, 4, 4, 4 }
    };

    vector<int> expected_outputs {
      8,
      11,
      0,
      1,
      1,
      -100,
      4
    };

    MostFrequentNumberEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= (int)inputs.size()) { return ""; }
      return "most_frequent_number(" + vector_to_string(inputs[i]) + ")";
    }

    int GetResult(int i){
      return most_frequent_number(inputs[i]);
    }

    float GetScore(int i, int result){
      return result == expected_outputs[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(expected_outputs[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  MostFrequentNumberEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
