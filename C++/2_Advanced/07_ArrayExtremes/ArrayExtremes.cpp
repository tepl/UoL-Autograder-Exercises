#include "ArrayExtremes.h"
#include <algorithm>
#include <map>
// import any required libraries here

using namespace std;

int sum_top_three(std::vector<int> a) {
    if(a.size() < 3){
        return 0;
    }
    std::sort(a.begin(), a.end(), std::greater<int>());
    return a[0] + a[1] + a[2];
}


int second_smallest(std::vector<int> a) {
    if(a.size() < 2){
        return 0;
    }
    std::sort(a.begin(), a.end());
    unsigned int i = 0;
    int m = a[0];
    while(i < a.size()){
        if(a[i] > m){
            return a[i];
        }
        i++;
    }
    return 0;
}


int most_frequent_number(std::vector<int> a) {
    if(a.size() <= 0){
        return 0;
    }

    std::map<int, int> dict;
    int max_count = 0;
    for(unsigned int i = 0; i < a.size(); i++){
        if(dict.count(a[i]) == 0){
            dict[a[i]] = 0;
        }
        dict[a[i]]++;
        if(dict[a[i]] > max_count){
            max_count = dict[a[i]];
        }
    }
    for(unsigned int i = 0; i < a.size(); i++){
        if(dict[a[i]] == max_count){
            return a[i];
        }
    }

    return 0;
}


int element_only_once(std::vector<int> a) {
    if(a.size() <= 0){
        return 0;
    }

    std::map<int, int> dict;
    for(unsigned int i = 0; i < a.size(); i++){
        if(dict.count(a[i]) == 0){
            dict[a[i]] = 0;
        }
        dict[a[i]]++;
    }
    for(unsigned int i = 0; i < a.size(); i++){
        if(dict[a[i]] == 1){
            return a[i];
        }
    }
    return 0;
}
