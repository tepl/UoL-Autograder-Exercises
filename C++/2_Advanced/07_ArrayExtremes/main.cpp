// YOU MAY MODIFY THIS FILE, AS LONG AS YOU DON'T MODIFY THE API DEFINED IN THE .H FILE
#include <iostream>
#include <cstdio>
#include <string.h>
#include "ArrayExtremes.h"

using namespace std;

int main() {
    int r;

    // Q1 - sum_top_three:
    vector<int> a1{5, 8, 9, 3, 6, 0, 7};
    
    r = sum_top_three(a1);
    
    printf("Q1: The result of sum_top_three is: %d\n", r);


    // Q2 - second_smallest:
    vector<int> a2{1, 2, 4, 3, 5, 6};

    r = second_smallest(a2);

    printf("Q2: The result of second_smallest is: %d\n", r);


    // Q3 - most_frequent_number
    vector<int> a3{5, 8, 7, 3, 8, 5, 2, 8};

    r = most_frequent_number(a3);

    printf("Q3: The result of most_frequent_number is: %d\n", r);


    // Q4 - element_only_once:
    vector<int> a4{1, 1, 3, 1, 1, 1};
    
    r = element_only_once(a4);

    printf("Q4: The result of element_only_once is: %d\n", r);

}
