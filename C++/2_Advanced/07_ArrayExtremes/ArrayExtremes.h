#ifndef Q7_H
#define Q7_H

#include <vector>

using namespace std;

// DO NOT MODIFY THIS FILE!
// THIS FILE DEFINES THE STRUCTURE OF THE FUNCTIONS WHICH WILL BE TESTED
// IF YOU MODIFY THE STRUCUTRE OF THE FUNCTION, YOUR RESULTS WILL NOT BE VALID


// API: Given a list of numbers a, find the three largest numbers in the array, and return their sum
//  It the list has fewer than 3 numbers, return 0
// Params:
//  a - a vector of integers
// Returns:
//  the sum of the three largest numbers in the array
// Example:
// a - [5, 8, 9, 3, 6, 0, 7], return - 24
// a - [32, 85, 64, -10, 64, 100], return - 249
// a - [1, 10], return 0
int sum_top_three(std::vector<int> a);

// API: Given a list of numbers a, return the second smallest number in the list
//  If the list has fewer than 2 numbers, return 0
//  Note: return the second smallest by value. If all have the same value, return 0
// Params:
//  a - a vector of integers
// Returns:
//  the second smallest number
// Example:
// a - [1, 2, 4, 3, 5, 6], return - 2
// a - [10, 10, 38, 10, 80, 64], return - 38
// a - [], return 0
int second_smallest(std::vector<int> a);

// API: Given a list of numbers a, find the number that occurs the most times in the array
//  If the list has no values, return 0
//  If the list has multiple numbers that occur the same number of times, return the one that occured the first
// Params:
//  a - a vector of integers
// Returns:
//  the number that occurs the most times in the array
// Example:
// a - [5, 8, 7, 3, 8, 5, 2, 8], return - 8
// a - [11, 85, 8, 95, 85, 51, 31, 11], return - 11
// a - [], return 0
int most_frequent_number(std::vector<int> a);

// API: Given a list of numbers a return the first number that occurs only once
//  If the list has no values, return 0
//  If no element occurs only once, return 0
// Params:
//  a - a vector of integers
// Returns:
//  the first number that only occurs once
// Example:
// a - [1, 1, 3, 1, 1, 1], return - 3
// a - [10, 65, 85, 10, 85, 47, 64, 10, 43, 65], return - 47
// a - [], return 0
int element_only_once(std::vector<int> a);

#endif // Q7_H