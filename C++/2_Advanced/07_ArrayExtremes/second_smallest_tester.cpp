#include "ArrayExtremes.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

class SecondSmallestEvaluator : public Evaluator<int>{
  public:
    vector<vector<int>> inputs {
      { 1, 2, 4, 3, 5, 6 },
      { 10, 10, 38, 10, 80, 64 },
      { },
      { 1, 10 },
      { 1, 1, 1, 1, 1 },
      { 1, 1, 1, 1, 2 },
      { -100, -100, -8, -3, -5, -20, -61 },
      { 6, 5, 4, 3, 2, 1, 0 }
    };

    vector<int> expected_outputs {
      2,
      38,
      0,
      10,
      0,
      2,
      -61,
      1
    };
    
    SecondSmallestEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= (int)inputs.size()) { return ""; }
      return "second_smallest(" + vector_to_string(inputs[i]) + ")";
    }

    int GetResult(int i){
      return second_smallest(inputs[i]);
    }

    float GetScore(int i, int result){
      return result == expected_outputs[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(expected_outputs[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  SecondSmallestEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
