#include "ArrayExtremes.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

class MosElementOnlyOnceEvaluator : public Evaluator<int>{
  public:
    vector<vector<int>> inputs {
      { 1, 1, 3, 1, 1, 1 },
      { 10, 65, 85, 10, 85, 47, 64, 10, 43, 65 },
      { },
      { 1, 10 },
      { 1, 1, 1, 1, 1 },
      { 1, 1, 1, 1, 2 },
      { 1, 2, 3, 4, 5, 6, 7 },
      { -100, -25, -6, -25, 100, -4, -4 }
    };

    vector<int> expected_outputs {
      3,
      47,
      0,
      1,
      0,
      2,
      1,
      -100
    };

    MosElementOnlyOnceEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= (int)inputs.size()) { return ""; }
      return "element_only_once(" + vector_to_string(inputs[i]) + ")";
    }

    int GetResult(int i){
      return element_only_once(inputs[i]);
    }

    float GetScore(int i, int result){
      return result == expected_outputs[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(expected_outputs[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  MosElementOnlyOnceEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
