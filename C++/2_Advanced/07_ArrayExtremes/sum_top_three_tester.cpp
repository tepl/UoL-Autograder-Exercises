#include "ArrayExtremes.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

class SumTopThreeEvaluator : public Evaluator<int>{
  public:
    vector<vector<int>> inputs {
      { 5, 8, 9, 3, 6, 0, 7 },
      { 32, 85, 64, -10, 64, 100 },
      { 1, 10 },
      { },
      { 0, 0, 0, 0, 0 },
      { -100, -100, -8, -3, -5, -20, -61 },
      { 3, 8, 100 }
    };

    vector<int> expected_outputs {
      24,
      249,
      0,
      0,
      0,
      -16,
      111
    };

    SumTopThreeEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= (int)inputs.size()) { return ""; }
      return "sum_top_three(" + vector_to_string(inputs[i]) + ")";
    }

    int GetResult(int i){
      return sum_top_three(inputs[i]);
    }

    float GetScore(int i, int result){
      return result == expected_outputs[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(expected_outputs[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  SumTopThreeEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
