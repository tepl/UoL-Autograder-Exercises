// YOU MAY MODIFY THIS FILE, AS LONG AS YOU DON'T MODIFY THE API DEFINED IN THE .H FILE
#include <iostream>
#include <cstdio>
#include <string.h>
#include "ArraySearch.h"

using namespace std;

int main() {
    int r, n;

    // Q1 - even_minus_odd:
    vector<int> a1{0, 1, 2, 3, 4, 5, 6};
    
    r = even_minus_odd(a1);
    
    printf("Q1: The result of even_minus_odd is: %d\n", r);


    // Q2 - incrementing_count:
    vector<int> a2{1, 2, 4, 3, 5, 6};

    r = out_of_place_item(a2);

    printf("Q2: The result of out_of_place_item is: %d\n", r);


    // Q3 - is_sum_of_two_primes
    vector<int> a3{1, 2, 3, 5, 6};

    r = incrementing_count(a3);

    printf("Q3: The result of incrementing_count is: %d\n", r);


    // Q4 - unique_pairs:
    vector<int> a4{1, 2, 4, 3, 5, 6};
    n = 6;
    
    r = unique_pairs(a4, n);

    printf("Q4: The result of unique_pairs is: %d\n", r);

}
