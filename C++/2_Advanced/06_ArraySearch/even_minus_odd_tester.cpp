#include "ArraySearch.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

class EvenMinusOddEvaluator : public Evaluator<int>{
  public:
    vector<vector<int>> inputs {
      { 0, 1, 2, 3, 4, 5, 6 },
      { 11, 37, 3, 6 },
      { },
      { 0, 0, 0, 0, 0 },
      { 1, 3, 5, 7 },
      { 56, 2, 67, 70, 74, 96, 44 },
      { 86, 7, 89, 36, 31, 95, 25 },
      { 0, -71, -89, -14, -37, -22, -82, -99, -2 },
    };

    vector<int> expected_outputs {
      1,
      -2,
      0,
      5,
      -4,
      5,
      -3,
      1
    };

    EvenMinusOddEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= (int)inputs.size()) { return ""; }
      return "even_minus_odd(" + vector_to_string(inputs[i]) + ")";
    }

    int GetResult(int i){
      return even_minus_odd(inputs[i]);
    }

    float GetScore(int i, int result){
      return result == expected_outputs[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(expected_outputs[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  EvenMinusOddEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
