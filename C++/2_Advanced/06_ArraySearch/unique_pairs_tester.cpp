#include "ArraySearch.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>
#include <tuple>

using namespace std;

class UniquePairTesterEvaluator : public Evaluator<int>{
  public:
    vector<vector<int>> a_inputs {
      { 1, 2, 3, 3, 3, 5, 6 },
      { 10, 28, 37, 4, 1, 1, 34 },
      { },
      { 1, 1, 1 },
      { -10, -52, 98, -2, 40, -12, 0 }, 
      { 1, 3, 3, 3, 1 },
      { 1, 3, 5, -1, 0, -5, -1 },
    };

    vector<int> m_inputs {
      6,
      38,
      1,
      3,
      -12,
      4,
      0
    };

    vector<int> expected_outputs {
      2,
      3,
      0,
      0,
      3,
      1,
      2
    };

    UniquePairTesterEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= (int)a_inputs.size()) { return ""; }
      return "unique_pairs(" + vector_to_string(a_inputs[i]) + ", " + to_string(m_inputs[i]) + ")";
    }

    int GetResult(int i){
      return unique_pairs(a_inputs[i], m_inputs[i]);
    }

    float GetScore(int i, int result){
      return result == expected_outputs[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(expected_outputs[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  UniquePairTesterEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
