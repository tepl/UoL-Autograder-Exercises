#include "ArraySearch.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

class IncrementingCountEvaluator : public Evaluator<int>{
  public:
    vector<vector<int>> inputs {
      { 1, 2, 3, 5, 6 },
      { 10, 11, 38, 38, 39, 40, 41, 16 },
      { 9, 8, 7, 6 },
      { },
      { 1, 1, 1, 1, 1 },
      { 4, 5, 6, 7, 8 },
      { 56, 57, 59, 60, 62, 63 },
      { -89, -88, -87, -86, -86, -85, -84, -83 },
    };

    vector<int> expected_outputs {
      3,
      4,
      0,
      0,
      0,
      5,
      2,
      4
    };

    IncrementingCountEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= (int)inputs.size()) { return ""; }
      return "incrementing_count(" + vector_to_string(inputs[i]) + ")";
    }

    int GetResult(int i){
      return incrementing_count(inputs[i]);
    }

    float GetScore(int i, int result){
      return result == expected_outputs[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(expected_outputs[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  IncrementingCountEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
