#include "ArraySearch.h"
// import any required libraries here


int even_minus_odd(std::vector<int> a) {
    int count = 0;
    for(unsigned int i = 0; i < a.size(); i++){
        count += a[i] % 2 == 0 ? 1 : -1;
    }
    return count;
}


int out_of_place_item(std::vector<int> a) {
    for(unsigned int i = 1; i < a.size(); i++){
        if(a[i] < a[i - 1]){ return a[i]; }
    }
    return 0;
}


int incrementing_count(std::vector<int> a) {
    int count = 0;
    int max_count = 0;
    for(unsigned int i = 1; i < a.size(); i++){
        if(a[i] - 1 == a[i - 1]) { 
            count++;
        }
        else {
            if(count > 0 && count + 1 > max_count){
                max_count = count + 1;
            }
            count = 0;
        }
    }
    if(count > 0 && count + 1 > max_count){
        max_count = count + 1;
    }
    return max_count;
}

bool array_contains(std::vector<int> a, int x, int y){
    for(unsigned int i = 0; i < a.size(); i++){
        if(a[i] == x || a[i] == y){ return true; }
    }
    return false;
}


int unique_pairs(std::vector<int> a, int n) {
    std::vector<int> pairs;
    for(unsigned int i = 0; i < a.size() - 1; i++){
        for(unsigned int j = i + 1; j < a.size(); j++){
            if(a[i] + a[j] == n && !array_contains(pairs, a[i], a[j])){
                pairs.push_back(a[i]);
            }
        }
    }
    return pairs.size();
}
