#include "ArraySearch.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

class OutOfPlaceItemEvaluator : public Evaluator<int>{
  public:
    vector<vector<int>> inputs {
      { 1, 2, 4, 3, 5, 6 },
      { 10, 11, 38, 10, 11, 38 },
      { },
      { 1, 1, 1, 1, 1 },
      { 1, 3, 5, 7 },
      { 7, 5, 4, 1 },
      { 56, 67, 70, 74, 96, 44 },
      { -89, -71, -37, -14, -22, -15, -8, -2 },
    };

    vector<int> expected_outputs {
      3,
      10,
      0,
      0,
      0,
      5,
      44,
      -22
    };

    OutOfPlaceItemEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= (int)inputs.size()) { return ""; }
      return "out_of_place_item(" + vector_to_string(inputs[i]) + ")";
    }

    int GetResult(int i){
      return out_of_place_item(inputs[i]);
    }

    float GetScore(int i, int result){
      return result == expected_outputs[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(expected_outputs[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  OutOfPlaceItemEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
