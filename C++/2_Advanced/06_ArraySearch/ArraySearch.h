#ifndef Q6_H
#define Q6_H

#include <vector>

using namespace std;

// DO NOT MODIFY THIS FILE!
// THIS FILE DEFINES THE STRUCTURE OF THE FUNCTIONS WHICH WILL BE TESTED
// IF YOU MODIFY THE STRUCUTRE OF THE FUNCTION, YOUR RESULTS WILL NOT BE VALID


// API: Given a list of numbers a, count the number of even and odd numbers and return the number of even numbers minus the number of odd numbers
//  If the list has no values, return 0
// Params:
//  a - a vector of integers
// Returns:
//  the number of even numbers in a minus the number of odd numbers in a
// Example:
// a - [0, 1, 2, 3, 4, 5, 6], return - 1
// a - [11, 37, 3, 6], return - -2
// a - [], return 0
int even_minus_odd(std::vector<int> a);

// API: Given a list of numbers a where each element is greater than equal to the previous, find the first number that is less than the previous number. Return the out of place number
//  If the list has no values, or there is no out of place number, return 0
//  If there are more than one number out of place, return the first one
// Params:
//  a - a vector of integers
// Returns:
//  the first element that is less than the previous
// Example:
// a - [1, 2, 4, 3, 5, 6], return - 3
// a - [10, 11, 38, 10, 11, 38], return - 10
// a - [], return 0
int out_of_place_item(std::vector<int> a);

// API: Given a list of numbers a, find the length of longest sequence of numbers that are strictly incrementing by 1
//  If the list has no values, return 0
// Params:
//  a - a vector of integers
// Returns:
//  number of elements that are consecutively greater than the previous by 1
// Example:
// a - [1, 2, 3, 5, 6], return - 3
// a - [10, 11, 38, 38, 39, 40, 41, 16], return - 4
// a - [9, 8, 7, 6], return 0
int incrementing_count(std::vector<int> a);

// API: Given a list of numbers a and a whole number n, find the number of unique pairs of numbers in a that add up to n
//  If the list has one or fewer values return 0
// Params:
//  a - a vector of integers
//  n - a whole number
// Returns:
//  number of unique pair of numbers in a that add up to a
// Example:
// a - [1, 2, 3, 3, 5, 6], n - 6, return - 2 (1+5, 3+3)
// a - [10, 28, 37, 3, 4, 1, 1, 10, 37, 34], n - 38, return - 3 (10+28, 37+1, 4+34)
// a - [], n = 1 return 0
int unique_pairs(std::vector<int> a, int n);

#endif // Q6_H