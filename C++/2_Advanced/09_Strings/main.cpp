// YOU MAY MODIFY THIS FILE, AS LONG AS YOU DON'T MODIFY THE API DEFINED IN THE .H FILE
#include <iostream>
#include <cstdio>
#include <string.h>
#include "Strings.h"

using namespace std;

int main() {
    bool r;
    string sr, s;
    char c;

    // Q1 - is_isogram:
    s = "bar";
    
    r = is_isogram(s);
    
    printf("Q1: The result of is_isogram is: %d\n", r);


    // Q2 - split:
    s = "alpha,beta,gamma";
    c = ',';

    sr = split(s, c);

    printf("Q2: The result of split is: %s\n", sr.c_str());


    // Q3 - trim:
    s = "    Hello world!    ";

    sr = trim(s);

    printf("Q3: The result of span is: %s\n", sr.c_str());


    // Q4 - alphabetical:
    vector<string> slr{"bar", "hello", "foo", "Hello"};
    
    vector<string> ar = alphabetical(slr);

    printf("Q4: The result of alphabetical is:\n");
    for(unsigned int i = 0; i < ar.size(); i++) {
        printf("\t%s\n", ar[i].c_str());
    }

}
