#include "Strings.h"
#include "cpp_eval_util.h"
#include <string>
#include <stdlib.h>
#include <iostream>
#include <exception>

using namespace std;

class IsogramEvaluator : public Evaluator<bool>{
  public:
    vector<string> inputs {
      "bar",
      "foo",
      "",
      "AaBbCcDdEe",
      "aabbccddee",
      "%@*$£!",
      "ELEC1620",
      "2004"
    };

    vector<bool> expected_outputs {
      true,
      false,
      true,
      true,
      false,
      true,
      false,
      false
    };

    IsogramEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= (int)inputs.size()) { return ""; }
      return "is_isogram(\"" + inputs[i] + "\")";
    }

    bool GetResult(int i){
      return is_isogram(inputs[i]);
    }

    float GetScore(int i, bool result){
      return result == expected_outputs[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, bool result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(expected_outputs[i]) + (score >= 1.0f ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  IsogramEvaluator evaluator(argc, argv);
  return evaluator.Run();
}