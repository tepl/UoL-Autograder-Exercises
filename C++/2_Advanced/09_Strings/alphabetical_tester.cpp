#include "Strings.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

class AlphabeticalEvaluator : public Evaluator<vector<string>>{
  public:
    vector<vector<string>> inputs {
      {"bar", "hello", "foo", "Hello"},
      {},
      {"", ""},
      {"c", "C", "b", "B", "a", "A"},
      {"Lorem ipsum", "Lorem ipsum dolor sit", "Lorem", "Lorem ipsum dolor"},
      {"Given", "a", "list", "of", "strings", "s", "return", "the", "same", "list", "sorted", "alphabetcially"}
    };

    vector<vector<string>> expected_outputs {
      {"bar", "foo", "Hello", "hello"},
      {},
      {"", ""},
      {"A", "a", "B", "b", "C", "c"},
      {"Lorem", "Lorem ipsum", "Lorem ipsum dolor", "Lorem ipsum dolor sit"},
      {"a","alphabetcially","Given","list","list","of","return","s","same","sorted","strings","the"}
    };

    AlphabeticalEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= (int)inputs.size()) { return ""; }
      return "alphabetical(" + vector_to_string(inputs[i]) + ")";
    }

    vector<string> GetResult(int i){
      return alphabetical(inputs[i]);
    }

    float GetScore(int i, vector<string> result){
      return result == expected_outputs[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, vector<string> result, float score){
      return "Returned " + vector_to_string(result) + ", expecting " + vector_to_string(expected_outputs[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  AlphabeticalEvaluator evaluator(argc, argv);
  return evaluator.Run();
}