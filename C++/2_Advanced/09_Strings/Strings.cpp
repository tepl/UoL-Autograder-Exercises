#include "Strings.h"
#include <algorithm>

// import any required libraries here

bool is_isogram(string s) {
    if(s.empty()){
        return true;
    }
    for(unsigned int i = 0; i < s.length() - 1; i++){
        for(unsigned int j = i + 1; j < s.length(); j++){
            if(s[i] == s[j]){
                return false;
            }
        }
    }
    return true;
}

string split(string s, char c, int i) {
    size_t pos = 0;
    string token;
    vector<string> split {};
    while ((pos = s.find(c)) != std::string::npos) {
        token = s.substr(0, pos);
        split.push_back(token);
        s.erase(0, pos + 1);
    }
    split.push_back(s);
    if(i >= (int)split.size()){
        return "";
    }
    return split[i];
}

string trim(string s) {
    size_t first = s.find_first_not_of(' ');
    if (string::npos == first)
    {
        return "";
    }
    size_t last = s.find_last_not_of(' ');
    return s.substr(first, (last - first + 1));
}

std::vector<string> alphabetical(std::vector<string> s) {
    std::sort(s.begin(), s.end(), [](const std::string& a, const std::string& b) -> bool {
        for (size_t c = 0; c < a.size() && c < b.size(); c++) {
            if (std::tolower(a[c]) != std::tolower(b[c]))
                return (std::tolower(a[c]) < std::tolower(b[c]));
            if(a[c] != b[c])
                return (a[c] < b[c]);
        }
        return a.size() < b.size();
    });
    return s;
}
