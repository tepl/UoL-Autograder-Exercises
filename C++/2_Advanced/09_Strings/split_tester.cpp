#include "Strings.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

class SplitEvaluator : public Evaluator<string>{
  public:
    vector<vector<string>> inputs {
      {"alpha,beta,gamma", ","},
      {"Hello world!", "!"},
      {"alpha,beta,gamma", "!"},
      {"", ","},
      {"Hello world!", "o"},
      {"AAAaaaBBBbbbbCCCcccDDDddd", "C"},
      {"%@*$£!%", "%"},
      {"ELEC1620", "1"},
      {"foo bar dez boz", " "}
    };
    vector<int> input_nums {
      0,
      1,
      0,
      0,
      1,
      3,
      1,
      0,
      4
    };

    vector<string> expected_outputs {
      "alpha",
      "",
      "alpha,beta,gamma",
      "",
      " w",
      "cccDDDddd",
      "@*$£!",
      "ELEC",
      ""
    };

    SplitEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= (int)inputs.size()) { return ""; }
      return "split(\"" + inputs[i][0] + "\", '" + inputs[i][1] + "', " + to_string(input_nums[i]) + ")";
    }

    string GetResult(int i){
      return split(inputs[i][0], inputs[i][1][0], input_nums[i]);
    }

    float GetScore(int i, string result){
      return result == expected_outputs[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, string result, float score){
      return "Returned \"" + result + "\", expecting \"" + expected_outputs[i] + "\"" + (score >= 1.0f ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  SplitEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
