#include "Strings.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

class TrimEvaluator : public Evaluator<string>{
  public:
    vector<string> inputs {
      "    Hello world!    ",
      "",
      "from the right       ",
      "      from the left",
      "   from both ",
      "             ",
      "1            2"
    };

    vector<string> expected_outputs {
      "Hello world!",
      "",
      "from the right",
      "from the left",
      "from both",
      "",
      "1            2"
    };

    TrimEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= (int)inputs.size()) { return ""; }
      return "trim(\"" + inputs[i] + "\")";
    }

    string GetResult(int i){
      return trim(inputs[i]);
    }

    float GetScore(int i, string result){
      return result == expected_outputs[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, string result, float score){
      return "Returned \"" + result + "\", expecting \"" + expected_outputs[i] + "\"" + (score >= 1.0f ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  TrimEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
