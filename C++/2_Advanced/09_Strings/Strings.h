#ifndef Q9_H
#define Q9_H

#include <string>
#include <vector>

using namespace std;

// DO NOT MODIFY THIS FILE!
// THIS FILE DEFINES THE STRUCTURE OF THE FUNCTIONS WHICH WILL BE TESTED
// IF YOU MODIFY THE STRUCUTRE OF THE FUNCTION, YOUR RESULTS WILL NOT BE VALID


// API: An isogram is a word that has no repeating letters, consecutive or non-consecutive.
//      Implement a function that determines whether a string that contains only letters is an isogram.
//      Assume the empty string is an isogram.
//      Ignore letter case.
// Params:
//  s - a string of characters, with no whitespace
// Returns:
//  true if the word is an isogram
// Example:
// s = "bar", return - true
// s - "foo", return - false
bool is_isogram(string s);

// API: Given a string s, a character c and a number i, split s to multiple strings separated by c and return the ith one (index starting at 0)
//  If the separator c is not in s and i is 0 return s
//  If i is greater than the number split items, return an empty string ""
// Params:
//  s - a string
//  c - the separator
//  i - the split selector
// Returns:
//  the ith item in the split string
// Example:
// s - "alpha,beta,gamma", c - ',', i - 0, return - "alpha"
// s - "Hello world!", c - '!', i - 1 return - ""
string split(string s, char c, int i);

// API: given a string s remove all leading and trailing whitespace and return the trimmed string
//  In this case whitespace characters only include spaces (' ')
// Params:
//  s - a string
// Returns:
//  the string with the leading and trailing whitespace removed
// Example:
// s - "    Hello world!    ", return - "Hello world!"
// s - "", return ""
string trim(string s);

// API: Given a list of strings s return the same list sorted alphabetcially
// Params:
//  s - a list of strings
// Returns:
//  the same list sorted alphabetcially
// Example:
// s - ["bar", "hello", "foo", "Hello"], return - ["bar", "foo", "Hello", "hello"]
// s - [], return []
std::vector<string> alphabetical(std::vector<string> s);

#endif // Q9_H