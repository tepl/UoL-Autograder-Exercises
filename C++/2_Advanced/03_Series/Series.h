#ifndef Q3_H
#define Q3_H

// DO NOT MODIFY THIS FILE!
// THIS FILE DEFINES THE STRUCTURE OF THE FUNCTIONS WHICH WILL BE TESTED
// IF YOU MODIFY THE STRUCUTRE OF THE FUNCTION, YOUR RESULTS WILL NOT BE VALID


// API: For a given non-zero whole number n calculate the following series:
// (1) + (1+2) + (1+2+3) + (1+2+3+4) + ... + (1+2+3+4+...+n)
// Params:
//  n - the control number of the series (n > 0)
// Returns:
//  the sum of the series
// Example:
//  n - 5, return 35
int growing_series(int n);

// API: For a given non-zero whole number n calculate the following series:
// 1 - 2 + 3 - 4 + ... ± n
// Params:
//  n - the last number in the series (n > 0)
// Returns:
//  the result of the series
// Example:
//  n - 5, return 3
int alternating_series(int n);

// API: Use the Newton/Euler Convergence Transformation to calculate Pi
// π = 2 * sum( k! / (2k + 1)!! ) for 0 <= k <= n
// Params:
//  n - the upper limit of k in the formula (n >= 0)
// Returns:
//  the value of the transformation
// Example:
//  n - 3, return 3.047619048
// Other examples:
//  n - 0, return 0
//  n - 10, return 3.1411060216
double newton_euler_transform(int n);

// API: Calculate the Taylor Series approximation for cos(x) for a given x
// Params:
//  n - the number of terms in the series (including the zero terms)
//  a - the central point of the series
//  x - the input value of the approximated function
// Returns:
//  The value of the function at x
// Example:
//  n - 1
//  a - 0
//  x - 0
//  return - 1
// Other examples:
//  n - 5, a - 0, x - 1, returns 0.54166666666
//  n - 7, a - -1, x - 2, returns -0.17282160590
double taylor_series(int n, double a, double x);


#endif // Q3_H