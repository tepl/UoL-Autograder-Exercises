#include "Series.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 16
#define MARGIN 1e-6

class NewtonEulerTransformEvaluator : public Evaluator<double>{
  public:
    double test_cases[TEST_CASE_COUNT][2] = {
        {0, 2.0000000000},
        {1, 2.6666666667},
        {2, 2.9333333333},
        {3, 3.0476190476},
        {4, 3.0984126984},
        {5, 3.1215007215},
        {6, 3.1321567322},
        {7, 3.1371295371},
        {8, 3.1394696806},
        {9, 3.1405781697},
        {10, 3.1411060216},
        {11, 3.1413584725},
        {12, 3.1414796490},
        {13, 3.1415379932},
        {14, 3.1415661593},
        {15, 3.1415797881}
    };

    NewtonEulerTransformEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "newton_euler_transform(" + to_string(test_cases[i][0]) + ")";
    }

    double GetResult(int i){
      return newton_euler_transform(test_cases[i][0]);
    }

    float GetScore(int i, double result){
      return is_within_margin(result, test_cases[i][1], MARGIN) ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, double result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][1]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  NewtonEulerTransformEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
