#include "Series.h"
#include <cmath>

// import any required libraries here

int growing_series(int n){
    int s = 0;
    for(int i = 0; i < n; i++){
        s += (i + 1) * (n - i);
    }
    return s;
}

int alternating_series(int n){
    if(n % 2 == 0){
        return -n / 2;
    }
    else{
        return (n + 1) / 2;
    }
}

double factorial(int N){
    if(N <= 1)
        return 1;
    return (double)(N * factorial(N - 1));
}

double newton_euler_transform(int n){
    double s = 0;
    for(int k = 0; k <= n; k++){
        s += (std::pow(2, k) * std::pow(factorial(k), 2)) / factorial(2 * k + 1);
    }
    return 2 * s;
}

double taylor_series(int n, double a, double x){
    double v = 0;
    if(n > 0){
        v = std::cos(a);
        for(int i = 1; i < n; i++){
            double f = 0;
            switch(i % 4){
                case 0: f = std::cos(a); break;
                case 1: f = -std::sin(a); break;
                case 2: f = -std::cos(a); break;
                case 3: f = std::sin(a); break;
            }
            v += f * std::pow((x - a), i) / factorial(i);
        }
    }
    return v;
}

