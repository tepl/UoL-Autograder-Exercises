// YOU MAY MODIFY THIS FILE, AS LONG AS YOU DON'T MODIFY THE API DEFINED IN THE .H FILE
#include <iostream>
#include <cstdio>
#include <string.h>
#include "Series.h"


int main() {
    int n, r;
    double a, x, rd;
    
    // Q1 - growing_series:
    n = 5;
    
    r = growing_series(n);
    
    printf("Q1: The result of growing_series is: %d\n", r);


    // Q2 - alternating_series:
    n = 5;

    r = alternating_series(n);

    printf("Q2: The result of alternating_series is: %d\n", r);


    // Q3 - newton_euler_transform
    n = 14;

    rd = newton_euler_transform(n);

    printf("Q3: The result of newton_euler_transform is: %.10f\n", rd);


    // Q4 - taylor_series:
    n = 1;
    a = 0;
    x = 0;
    
    rd = taylor_series(n, a, x);

    printf("Q4: The result of taylor_series is: %.10f\n", rd);
}
