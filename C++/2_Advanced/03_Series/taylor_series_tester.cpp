#include "Series.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 16
#define MARGIN 1e-6

class TaylorSeriesEvaluator : public Evaluator<double>{
  public:
    double test_cases[TEST_CASE_COUNT][4] = {
        {1, 0, 0, 1},
        {5, 0, 1, 0.54166666666},
        {7, -1, 2, -0.17282160590},
        {7, -1.519751979900283, -6.322082123950947, -7.894630700385877},
        {4, 9.922156855186397, -2.82914928218712, 229.3555425904391},
        {7, 3.847727550374856, 3.7507860488014, -0.8201098288059057},
        {8, -2.908161719371734, -5.385304337894905, 0.6573689928937689},
        {9, -3.847218438473289, -0.8998363678692556, 0.6397668237834968},
        {9, -4.921331807034932, 0.7680732957686836, 15.10015013478306},
        {2, -2.948840020237135, -7.640122549073944, -1.880148712592586},
        {6, -6.878404835200751, 0.3377811389239795, 133.1823362824281},
        {5, -8.522606154389932, 8.052932200157169, -2447.818177116702},
        {2, -0.0615050110416, -1.226588879649542, 0.9264958373248453},
        {4, 2.371261734791716, 9.330034047175797, 50.92313214718453},
        {1, 1.29491649560377, 2.019590858687375, 0.2723936034522318},
        {6, -1.421590182061331, 6.578330798047773, 214.3086854765403}
    };

    TaylorSeriesEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "taylor_series(" + to_string(test_cases[i][0]) + ", " + to_string(test_cases[i][1]) + ", " + to_string(test_cases[i][2]) + ")";
    }

    double GetResult(int i){
      return taylor_series((int)test_cases[i][0], test_cases[i][1], test_cases[i][2]);
    }

    float GetScore(int i, double result){
      return is_within_margin(result, test_cases[i][3], MARGIN) ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, double result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][3]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  TaylorSeriesEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
