#include "Series.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 8

class GrowingSeriesEvaluator : public Evaluator<int>{
  public:
    int test_cases[TEST_CASE_COUNT][2] = {
      {5, 35},
      {49, 20825},
      {28, 4060},
      {13, 455},
      {39, 10660},
      {2, 4},
      {40, 11480},
      {32, 5984}
    };

    GrowingSeriesEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "growing_series(" + to_string(test_cases[i][0]) + ")";
    }

    int GetResult(int i){
      return growing_series(test_cases[i][0]);
    }

    float GetScore(int i, int result){
      return result == test_cases[i][1] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][1]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  GrowingSeriesEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
