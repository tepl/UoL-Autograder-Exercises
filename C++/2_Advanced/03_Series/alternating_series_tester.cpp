#include "Series.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 8

class AlternatingSeriesEvaluator : public Evaluator<int>{
  public:
    int test_cases[TEST_CASE_COUNT][2] = {
        {5, 3},
        {38, -19},
        {43, 22},
        {45, 23},
        {15, 8},
        {21, 11},
        {2, -1},
        {8, -4}
    };

    AlternatingSeriesEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "alternating_series(" + to_string(test_cases[i][0]) + ")";
    }

    int GetResult(int i){
      return alternating_series(test_cases[i][0]);
    }

    float GetScore(int i, int result){
      return result == test_cases[i][1] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][1]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  AlternatingSeriesEvaluator evaluator(argc, argv);
  return evaluator.Run();
}