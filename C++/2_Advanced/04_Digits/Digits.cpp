#include "Digits.h"
#include <vector>
#include <iostream>

// import any required libraries here

std::vector<int> to_digits(int n){
    std::vector<int> v = {};
    // https://stackoverflow.com/questions/15987666/converting-integer-into-array-of-digits
    while (n) { 
        v.push_back(n % 10);
        n /= 10;
    }
    
    return v;
}

int digit_product(int n){
    std::vector<int> v = to_digits(n);
    return v[0] * v[v.size() - 1];
}

int sum_digits(int n){
    std::vector<int> v = to_digits(n);
    int s = 0;
    for(int i = 0; i < v.size(); i++)
        s += v[i];
    return s;
}

int reverse_digits(int n){
    std::vector<int> v = to_digits(n);
    int s = 0;
    for(int i = 0; i < v.size(); i++)
        s = s * 10 + v[i];
    return s;
}

int most_frequent_digit(int n){
    int f[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    std::vector<int> v = to_digits(n);
    
    for(int i = 0; i < v.size(); i++)
        f[v[i]] += 1;
    
    int max = 0;
    for(int i = 0; i < 10; i++)
        if(f[i] > f[max])
            max = i;
            
    return max;
}