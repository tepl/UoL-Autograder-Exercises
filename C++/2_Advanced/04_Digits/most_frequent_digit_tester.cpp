#include "Digits.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 8

class MostFrequentDigitEvaluator : public Evaluator<int>{
  public:
    int test_cases[TEST_CASE_COUNT][2] = {
        {225675779, 7},
        {1122334400, 0},
        {16461, 1},
        {1, 1},
        {44444444, 4},
        {7288, 8},
        {526985216, 2},
        {6512378, 1}
    };

    MostFrequentDigitEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "most_frequent_digit(" + to_string(test_cases[i][0]) + ")";
    }

    int GetResult(int i){
      return most_frequent_digit(test_cases[i][0]);
    }

    float GetScore(int i, int result){
      return result == test_cases[i][1] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][1]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  MostFrequentDigitEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
