#include "Digits.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 8

class ReverseDigitsEvaluator : public Evaluator<int>{
  public:
    int test_cases[TEST_CASE_COUNT][2] = {
        {21435, 53412},
        {1, 1},
        {972, 279},
        {218743, 347812},
        {100000, 1},
        {47325, 52374},
        {9876543, 3456789},
        {651235, 532156}
    };

    ReverseDigitsEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "reverse_digits(" + to_string(test_cases[i][0]) + ")";
    }

    int GetResult(int i){
      return reverse_digits(test_cases[i][0]);
    }

    float GetScore(int i, int result){
      return result == test_cases[i][1] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][1]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  ReverseDigitsEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
