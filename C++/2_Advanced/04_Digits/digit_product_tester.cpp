#include "Digits.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 8

class DigitProductEvaluator : public Evaluator<int>{
  public:
    int test_cases[TEST_CASE_COUNT][2] = {
      {21435, 10},
      {1, 1},
      {30, 0},
      {35, 15},
      {654, 24},
      {972, 18},
      {218743, 6},
      {651235, 30}
    };

    DigitProductEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "digit_product(" + to_string(test_cases[i][0]) + ")";
    }

    int GetResult(int i){
      return digit_product(test_cases[i][0]);
    }

    float GetScore(int i, int result){
      return result == test_cases[i][1] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][1]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  DigitProductEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
