// YOU MAY MODIFY THIS FILE, AS LONG AS YOU DON'T MODIFY THE API DEFINED IN THE .H FILE
#include <iostream>
#include <cstdio>
#include <string.h>
#include "Digits.h"


int main() {
    int n, r;
    
    // Q1 - digit_product:
    n = 21435;
    
    r = digit_product(n);
    
    printf("Q1: The result of digit_product is: %d\n", r);


    // Q2 - sum_digits:
    n = 21435;

    r = sum_digits(n);

    printf("Q2: The result of sum_digits is: %d\n", r);


    // Q3 - reverse_digits
    n = 21435;

    r = reverse_digits(n);

    printf("Q3: The result of reverse_digits is: %d\n", r);


    // Q4 - most_frequent_digit:
    n = 225675779;
    
    r = most_frequent_digit(n);

    printf("Q4: The result of most_frequent_digit is: %d\n", r);
}
