#ifndef Q4_H
#define Q4_H

// DO NOT MODIFY THIS FILE!
// THIS FILE DEFINES THE STRUCTURE OF THE FUNCTIONS WHICH WILL BE TESTED
// IF YOU MODIFY THE STRUCUTRE OF THE FUNCTION, YOUR RESULTS WILL NOT BE VALID


// API: Given a positive whole number n, find the product of the first and last digit of n
// Params:
//  n - the number
// Returns:
//  the product of the first and last digits
// Example:
// n - 21435, return - 10
int digit_product(int n);

// API: Given a positive whole number n, find the sum of all the digits of n
// Params:
//  n - the number
// Returns:
//  the sum of all the digits
// Example:
//  n - 21435, return - 15
int sum_digits(int n);

// API: Given a positive whole number n, return a new number, that is made up of the same digits as n, but in a reverse order
// Params:
//  n - the number
// Returns:
//  the new number with the digits of n in a reverse order
// Example:
//  n - 21435, return - 53412
int reverse_digits(int n);

// API: Given a positive whole number n, find the most frequent digit of n
// If the multiple digits occur the same number of times, return the lowest
// Params:
//  n - the number
// Returns:
//  the digit that occurs the most in n
// Example:
//  n - 225675779, return - 7
// Other example:
//  n - 1122334400, return - 0
int most_frequent_digit(int n);


#endif // Q4_H