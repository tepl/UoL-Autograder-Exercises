#include "Primes.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 10

class NthPrimeEvaluator : public Evaluator<int>{
  public:
    int test_cases[TEST_CASE_COUNT][2] = {
        {1, 2},
        {2, 3},
        {10, 29},
        {0, 0},
        {-10, 0},
        {21, 73},
        {105, 571},
        {999, 7907},
        {1000, 7919},
        {1001, 0}
    };

    NthPrimeEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "nth_prime(" + to_string(test_cases[i][0]) + ")";
    }

    int GetResult(int i){
      return nth_prime(test_cases[i][0]);
    }

    float GetScore(int i, int result){
      return result == test_cases[i][1] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][1]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  NthPrimeEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
