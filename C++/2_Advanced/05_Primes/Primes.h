#ifndef Q5_H
#define Q5_H

// DO NOT MODIFY THIS FILE!
// THIS FILE DEFINES THE STRUCTURE OF THE FUNCTIONS WHICH WILL BE TESTED
// IF YOU MODIFY THE STRUCUTRE OF THE FUNCTION, YOUR RESULTS WILL NOT BE VALID


// API: Given a positive whole number n, determine if it's a prime number
//  A prime number is one that has exactly two divisors, 1 and itself
//  If n < 0 return false
// Params:
//  n - the number
// Returns:
//  true if n is a prime number
// Example:
// n - 17, return - true
// n - 4, return - false
bool is_prime(int n);

// API: Given a positive whole number n, find the nth prime number.
//  If n < 1 or n > 1000 return 0
// Params:
//  n - the number
// Returns:
//  the nth prime number
// Example:
//  n - 1, return - 2
//  n - 2, return - 3
//  n - 10, return - 29
int nth_prime(int n);

// API: Given a positive whole number n, return true if it can be expressed as the sum of two prime numbers
//  If n < 1 return false
// Params:
//  n - the number
// Returns:
//  true if the number can be expressed as the sum of two primes
// Example:
//  n - 20, return - true (3 + 17 or 7 + 13)
//  n - 11, return - false
bool is_sum_of_two_primes(int n);

// API: Given a positive whole number n, count the number of prime factors
//  Prime factors are prime numbers that can be multiplied to give the original number
//  if n < 1 return 0
// Params:
//  n - the number
// Returns:
//  the number of prime factors
// Example:
//  n - 315, return - 4 (3, 3, 5, 7)
//  n - 1485, return - 5 (3, 3, 3, 5, 11)
int count_prime_factors(int n);

// API: Given positive whole numbers n and m, determine if they are coprimes of each other
//  Coprimes are numbers which only have a single common divisor (1)
//  if n < 1 or m < 1 return false
// Params:
//  n - the number
//  m - the other number
// Returns:
//  true if n and m has exactly one common factor
// Example:
//  n - 5, m = 6, return - true
//  n - 8, m = 16, return - false
bool are_coprimes(int n, int m);

// API: Given positive prime numbers p and q, let n = p * q and calculate the number of coprimes n has, that are less than n
//  if p < 1 or q < 1 or if it's not a prime number return 0
// Params:
//  p - one of the prime numbers
//  q - the other prime number
// Returns:
//  the number of coprimes p * q have that are less than p * q
// Example:
//  p - 2, q - 7, return - 6 (1, 3, 5, 9, 11, 13)
//  p - 3, q - 11, return - 20 (1, 2, 4, 5, 7, 8, 10, 13, 14, 16, 17, 19, 20, 23, 25, 26, 28, 29, 31, 32)
//  p - 4, q = 13, return - 0 (4 is not a prime number)
int number_of_coprimes(int p, int q);


#endif // Q5_H