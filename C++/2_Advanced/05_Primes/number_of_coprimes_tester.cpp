#include "Primes.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 16

class NumberOfCoprimesEvaluator : public Evaluator<int>{
  public:
    int test_cases[TEST_CASE_COUNT][3] = {
        {2, 7, 6},
        {3, 11, 20},
        {4, 13, 0},
        {5, 3, 8},
        {5, 6, 0},
        {8, 16, 0},
        {1, 0, 0},
        {-10, 5, 0},
        {3, 0, 0},
        {101, 101, 10000},
        {71, 7, 420},
        {149, 29, 4144},
        {2, 211, 210},
        {1559, 11, 15580},
        {19, 53, 936},
        {71, 29, 1960}
    };

    NumberOfCoprimesEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "number_of_coprimes(" + to_string(test_cases[i][0]) + ", " + to_string(test_cases[i][1]) + ")";
    }

    int GetResult(int i){
      return number_of_coprimes(test_cases[i][0], test_cases[i][1]);
    }

    float GetScore(int i, int result){
      return result == test_cases[i][2] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][2]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  NumberOfCoprimesEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
