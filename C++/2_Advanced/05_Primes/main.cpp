// YOU MAY MODIFY THIS FILE, AS LONG AS YOU DON'T MODIFY THE API DEFINED IN THE .H FILE
#include <iostream>
#include <cstdio>
#include <string.h>
#include "Primes.h"


int main() {
    int n, r, m;
    
    // Q1 - is_prime:
    n = 17;
    
    r = is_prime(n);
    
    printf("Q1: The result of is_prime is: %d\n", r);


    // Q2 - nth_prime:
    n = 10;

    r = nth_prime(n);

    printf("Q2: The result of nth_prime is: %d\n", r);


    // Q3 - is_sum_of_two_primes
    n = 20;

    r = is_sum_of_two_primes(n);

    printf("Q3: The result of is_sum_of_two_primes is: %d\n", r);


    // Q4 - count_prime_factors:
    n = 315;
    
    r = count_prime_factors(n);

    printf("Q4: The result of count_prime_factors is: %d\n", r);


    // Q5 - are_coprimes:
    n = 5;
    m = 6;
    
    r = are_coprimes(n, m);

    printf("Q4: The result of are_coprimes is: %d\n", r);


    // Q6 - number_of_coprimes:
    n = 2;
    m = 7;
    
    r = number_of_coprimes(n, m);

    printf("Q4: The result of number_of_coprimes is: %d\n", r);
}
