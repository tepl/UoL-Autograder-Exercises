#include "Primes.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 12

class IsPrimeEvaluator : public Evaluator<int>{
  public:
    int test_cases[TEST_CASE_COUNT][2] = {
        {17, 1},
        {4, 0},
        {2, 1},
        {3, 1},
        {7, 1},
        {9, 0},
        {5003, 1},
        {5005, 0},
        {971, 1},
        {1, 0},
        {-10, 0},
        {-7, 0}
  };

    IsPrimeEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "is_prime(" + to_string(test_cases[i][0]) + ")";
    }

    int GetResult(int i){
      return is_prime(test_cases[i][0]);
    }

    float GetScore(int i, int result){
      return result == test_cases[i][1] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][1]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  IsPrimeEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
