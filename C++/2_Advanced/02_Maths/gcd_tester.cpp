#include "Maths.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 16

class GCDEvaluator : public Evaluator<int>{
  public:
    int test_cases[TEST_CASE_COUNT][3] = {
        {8, 12, 4},
        {17, 29, 1},
        {8, 10, 2},
        {22, 1, 1},
        {18, 24, 6},
        {20, 24, 4},
        {15, 12, 3},
        {26, 26, 26},
        {4, 24, 4},
        {25, 20, 5},
        {18, 10, 2},
        {6, 2, 2},
        {2, 21, 1},
        {21, 27, 3},
        {21, 18, 3},
        {28, 7, 7}
    };

    GCDEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "gcd(" + to_string(test_cases[i][0]) + ", " + to_string(test_cases[i][1]) + ")";
    }

    int GetResult(int i){
      return gcd(test_cases[i][0], test_cases[i][1]);
    }

    float GetScore(int i, int result){
      return result == test_cases[i][2] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][2]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  GCDEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
