#include "Maths.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 8

class SumOddEvaluator : public Evaluator<int>{
  public:
    int test_cases[TEST_CASE_COUNT][2] = {
      {5, 25},
      {37, 1369},
      {45, 2025},
      {51, 2601},
      {54, 2916},
      {21, 441},
      {76, 5776},
      {0, 0}
  };

    SumOddEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "sum_odd(" + to_string(test_cases[i][0]) + ")";
    }

    int GetResult(int i){
      return sum_odd(test_cases[i][0]);
    }

    float GetScore(int i, int result){
      return result == test_cases[i][1] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][1]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  SumOddEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
