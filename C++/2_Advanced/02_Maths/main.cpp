// YOU MAY MODIFY THIS FILE, AS LONG AS YOU DON'T MODIFY THE API DEFINED IN THE .H FILE
#include <iostream>
#include <cstdio>
#include <string.h>
#include "Maths.h"


int main() {
    int a, b, n, r;
    unsigned long long rf;
    
    // Q1 - sum_odd:
    n = 5;
    
    r = sum_odd(n);
    
    printf("Q1: The result of sum_odd is: %d\n", r);


    // Q2 - factorial:
    n = 20;

    rf = factorial(n);

    printf("Q2: The result of factorial is: %llu\n", rf);


    // Q3 - gcd:
    a = 8;
    b = 12;

    r = gcd(a, b);

    printf("Q3: The result of gcd is: %d\n", r);


    // Q4 - scm
    a = 15;
    b = 25;

    r = scm(a, b);

    printf("Q4: The result of scm is: %d\n", r);
}
