#include "Maths.h"
#include <stdlib.h>

// import any required libraries here

int sum_odd(int N){
    int s = 0;
    for(int i = 0; i < N; i++){
        s += (1 + i * 2);
    }
    return s;
}

unsigned long long factorial(int N){
    if(N <= 1){
        return 1;
    }
    return N * factorial(N - 1);
    // int p = 1;
    // for(int i = 1; i <= N; i++){
    //     p *= i;
    // }
    // return p;
}

int pow(int x, int p) {
  if (p == 0) return 1;
  if (p == 1) return x;
  return x * pow(x, p-1);
}

int gcd(int a, int b){
    int d = 0;
    while(a % 2 == 0 && b % 2 == 0){
        a /= 2;
        b /= 2;
        d += 1;
    }
    while(a != b){
        if(a % 2 == 0)
            a /= 2;
        else if(b % 2 == 0)
            b /= 2;
        else if (a > b)
            a = (a - b) / 2;
        else
            b = (b - a) / 2;
    }
    return a * pow(2, d);
}

int scm(int a, int b){
    return abs(a * b) / gcd(a, b);
}