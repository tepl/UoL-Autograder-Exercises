#ifndef Q2_H
#define Q2_H

// DO NOT MODIFY THIS FILE!
// THIS FILE DEFINES THE STRUCTURE OF THE FUNCTIONS WHICH WILL BE TESTED
// IF YOU MODIFY THE STRUCUTRE OF THE FUNCTION, YOUR RESULTS WILL NOT BE VALID

// API: For a given non-zero whole number N return the sum of the first N odd numbers
// Params:
//  N - the number of odd numbers to sum
// Returns:
//  The sum of the numbers
// Example:
//  N - 5, return - 25
int sum_odd(int N);

// API: For a given non-zero whole number N return the factorial of N (1 * 2 * 3 * ... N)
// if N is 0, return 1
// Params:
//  N - the number to calculate factorial for (n <= 20)
// Returns:
//  The factorial of the number
// Example:
//  N - 5, return - 120
unsigned long long factorial(int N);

// API: for two positive whole numbers a and b, calculate the greatest common divisor
// Params:
//  a - first number
//  b - second number
// Returns:
//  the greatest common divisor
// Example:
//  a - 8, b - 12, return - 4
int gcd(int a, int b);

// API: for two positive whole numbers a and b, calculate the smallest common multiple
// Params:
//  a - first number
//  b - second number
// Returns:
//  the smallest common multiple of a and b
// Example:
//  a - 15, b - 25, return - 75
int scm(int a, int b);

#endif // Q2_H