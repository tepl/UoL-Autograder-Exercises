#include "Maths.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 8

class FactorialEvaluator : public Evaluator<unsigned long long>{
  public:
    unsigned long long test_cases[TEST_CASE_COUNT][2] = {
        {0, 1},
        {1, 1},
        {2, 2},
        {5, 120},
        {10, 3628800},
        {12, 479001600},
        {15, 1307674368000},
        {20, 2432902008176640000}
    };


    FactorialEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "factorial(" + to_string(test_cases[i][0]) + ")";
    }

    unsigned long long GetResult(int i){
      return factorial(test_cases[i][0]);
    }

    float GetScore(int i, unsigned long long result){
      return result == test_cases[i][1] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, unsigned long long result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][1]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  FactorialEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
