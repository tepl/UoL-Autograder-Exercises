#include "Maths.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 16

class SCMEvaluator : public Evaluator<int>{
  public:
    int test_cases[TEST_CASE_COUNT][3] = {
        {15, 25, 75},
        {24, 36, 72},
        {21, 31, 651},
        {9, 19, 171},
        {34, 38, 646},
        {14, 23, 322},
        {8, 15, 120},
        {30, 1, 30},
        {20, 21, 420},
        {38, 19, 38},
        {4, 10, 20},
        {24, 25, 600},
        {35, 30, 210},
        {18, 27, 54},
        {18, 20, 180},
        {25, 20, 100}
    };

    SCMEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "scm(" + to_string(test_cases[i][0]) + ", " + to_string(test_cases[i][1]) + ")";
    }

    int GetResult(int i){
      return scm(test_cases[i][0], test_cases[i][1]);
    }

    float GetScore(int i, int result){
      return result == test_cases[i][2] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][2]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  SCMEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
