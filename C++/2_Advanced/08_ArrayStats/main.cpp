// YOU MAY MODIFY THIS FILE, AS LONG AS YOU DON'T MODIFY THE API DEFINED IN THE .H FILE
#include <iostream>
#include <cstdio>
#include <string.h>
#include "ArrayStats.h"

using namespace std;

int main() {
    double r;

    // Q1 - average:
    vector<double> a1{1.2, 8.3, 5.0, 1.52, 10.3, 3.14};
    
    r = average(a1);
    
    printf("Q1: The result of average is: %.3f\n", r);


    // Q2 - median:
    vector<double> a2{3.8, 4.2, 0.3, 0.3, 0.1, 0.08, 5.6};

    r = median(a2);

    printf("Q2: The result of median is: %.3f\n", r);


    // Q3 - span
    vector<double> a3{1.2, 8.3, 5.0, 1.52, 10.3, 3.14};

    r = span(a3);

    printf("Q3: The result of span is: %.3f\n", r);


    // Q4 - stddev:
    vector<double> a4{1.2, 8.3, 5.0, 1.52, 10.3, 3.14};
    
    r = stddev(a4);

    printf("Q4: The result of stddev is: %.5f\n", r);

}
