#include "ArrayStats.h"

#include <algorithm>
#include <math.h>

// import any required libraries here

double average(std::vector<double> a) {
    if(a.size() <= 0) {
        return 0;
    }
    double s = 0;

    for(unsigned int i = 0; i < a.size(); i++){
        s += a[i];
    }
    return s / a.size();
}


double median(std::vector<double> a) {
    if(a.size() <= 0){
        return 0;
    }
    std::sort(a.begin(), a.end());
    unsigned int i = a.size() / 2;
    if(a.size() % 2 == 0){
        return (a[i] + a[i - 1]) / 2.0;
    }
    else{
        return a[i];
    }
}


double span(std::vector<double> a) {
    if(a.size() <= 0){
        return 0;
    }
    std::sort(a.begin(), a.end());
    return a[a.size() - 1] - a[0];
}


double stddev(std::vector<double> a) {
    if(a.size() <= 0){
        return 0;
    }

    double s = 0;
    double avg = average(a);
    for(unsigned int i = 0; i < a.size(); i++){
        s += pow((a[i] - avg), 2);
    }
    return sqrt(s / a.size());
}
