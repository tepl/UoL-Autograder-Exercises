#include "ArrayStats.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define MARGIN 1e-6

class StddevEvaluator : public Evaluator<double>{
  public:
    vector<vector<double>> inputs {
      { 1.2, 8.3, 5.0, 1.52, 10.3, 3.14 },
      { },
      { 0, 0, 0, 0, 0 },
      { 3.8, 4.2, 0.3, 0.3, 0.1, 0.08, 5.6 },
      { 5.006,7.461,3.758,0.874,3.376,8.523,1.846,2.783,8.973,6.366 },
      { 6.800,5.935,14.856,2.483,0.723,9.579,5.863,7.553 },
      { -2.504,2.603,2.900,-1.904,-1.944,2.762,0.003,1.948,1.034,-1.975,3.355 }
    };

    vector<double> expected_outputs {
      3.389725,
      0,
      0,
      2.206994,
      2.684743,
      4.036326,
      2.193505
    };
    

    StddevEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= (int)inputs.size()) { return ""; }
      return "stddev(" + vector_to_string(inputs[i]) + ")";
    }

    double GetResult(int i){
      return stddev(inputs[i]);
    }

    float GetScore(int i, double result){
      return is_within_margin(result, expected_outputs[i], MARGIN) ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, double result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(expected_outputs[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  StddevEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
