#ifndef Q8_H
#define Q8_H

#include <vector>

using namespace std;

// DO NOT MODIFY THIS FILE!
// THIS FILE DEFINES THE STRUCTURE OF THE FUNCTIONS WHICH WILL BE TESTED
// IF YOU MODIFY THE STRUCUTRE OF THE FUNCTION, YOUR RESULTS WILL NOT BE VALID


// API: Given a list of numbers a, calculate the average of the array
//  If the list is empty, return 0
// Params:
//  a - a vector of numbers
// Returns:
//  the average of the numbers
// Example:
// a - [1.2, 8.3, 5.0, 1.52, 10.3, 3.14], return - 4.91
// a - [], return 0
double average(std::vector<double> a);

// API: Given a list of numbers a, calculate the median of the array
//  If the list is empty, return 0
//  The median is the middle value of the sorted array. If the list has even number of elements return the average of the two middle elements
// Params:
//  a - a vector of numbers
// Returns:
//  the median of the numbers
// Example:
// a - [3.8, 4.2, 0.3, 0.3, 0.1, 0.08, 5.6], return - 0.3
// a - [1.2, 8.3, 5.0, 1.52, 10.3, 3.14], return - 4.07
// a - [], return 0
double median(std::vector<double> a);

// API: Given a list of numbers a, calculate the span of the array
//  If the list is empty, return 0
//  Note: the span is the difference between the smallest and largest number
// Params:
//  a - a vector of numbers
// Returns:
//  the span of the numbers
// Example:
// a - [1.2, 8.3, 5.0, 1.52, 10.3, 3.14], return - 9.1
// a - [], return 0
double span(std::vector<double> a);

// API: Given a list of numbers a, calculate the standard deviation of the array
//  If the list is empty, return 0
// Params:
//  a - a vector of numbers
// Returns:
//  the first number that only occurs once
// Example:
// a - [1.2, 8.3, 5.0, 1.52, 10.3, 3.14], return - 3.389724669
// a - [], return 0
double stddev(std::vector<double> a);

#endif // Q8_H