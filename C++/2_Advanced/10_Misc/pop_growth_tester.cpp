#include "Misc.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

class PopGrowthEvaluator : public Evaluator<int>{
  public:
    vector<vector<double>> inputs {
      {100,10,5,200},
      {100, 0, 0, 50},
      {100, 20, -10, 2000},
      {10, -10, 10, 50},
      {10, -10, 10, 100},
      {10, 0, -10, 5},
      {200, 0.1, 0, 4000000},
      {1000, -1, 0, 1001}
    };
    
    vector<int> expected_outputs {
      6, 0, 21, 6, -1, 0, 9909, -1
    };

    PopGrowthEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= (int)inputs.size()) { return ""; }
      return "pop_growth(" + to_string(inputs[i][0]) + ", " + to_string(inputs[i][1]) + ", " + to_string(inputs[i][2]) + ", " + to_string(inputs[i][3]) + ")";
    }

    int GetResult(int i){
      return pop_growth(inputs[i][0], inputs[i][1], inputs[i][2], inputs[i][3]);
    }

    float GetScore(int i, int result){
      return result == expected_outputs[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(expected_outputs[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  PopGrowthEvaluator evaluator(argc, argv);
  return evaluator.Run();
}