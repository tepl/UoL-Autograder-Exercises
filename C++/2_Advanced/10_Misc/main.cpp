// YOU MAY MODIFY THIS FILE, AS LONG AS YOU DON'T MODIFY THE API DEFINED IN THE .H FILE
#include <iostream>
#include <cstdio>
#include <string.h>
#include "Misc.h"

using namespace std;

void PrintArray(vector<int> arr){
    for(unsigned int i = 0; i < arr.size(); i++) {
        printf("\t%d\n", arr[i]);
    }
}

int main() {
    bool r;
    string sr, s;
    char c;

    // Q1 - is_isogram:
    vector<int> l1 = {1, 9, 6, -5, 0, 8, 11, 1, 2};
    
    vector<int> r1 = selective_sort(l1);
    
    printf("Q1: The result of selective_sort is:\n");
    PrintArray(r1);


    // Q2 - split:
    vector<int> l2 = {1,2,3,1,2,1,2,3};
    int n2 = 2;

    vector<int> r2 = delete_nth(l2, n2);

    printf("Q2: The result of split delete_nth:\n");
    PrintArray(r2);


    // Q3 - trim:
    double p0 = 100.0f;
    double g = 10.0f;
    double n = 5.0f;
    double p = 200.0f;

    int r3 = pop_growth(p0, g, n, p);

    printf("Q3: The result of pop_growth is: %d\n", r3);


    // Q4 - alphabetical:
    double a = 5.0f;
    double b = 4.0f;
    double c = 3.0f;

    bool r4 = is_triangle(a, b, c);

    printf("Q4: The result of is_triangle is: %d\n", r4);

}
