#include "Misc.h"
#include <algorithm>
#include <map>

// import any required libraries here

vector<int> selective_sort(vector<int> l) {
    vector<int> odds = {};
    for(int i = 0; i < (int)l.size(); i++){
        if(l[i] % 2 != 0){
            odds.push_back(l[i]);
        }
    }

    std::sort(odds.begin(), odds.end(), std::less<int>());
    int counter = 0;

    for(int i = 0; i < (int)l.size(); i++){
        if(l[i] % 2 != 0){
            l[i] = odds[counter];
            counter++;
        }
    }
    return l;
}

vector<int> delete_nth(vector<int> l, int n) {
    vector<int> limited = {};
    if(n <= 0) { return limited; }
    
    std::map<int, int> dict;
    for(int i = 0; i < (int)l.size(); i++) {
        if(dict.count(l[i]) <= 0) {
            dict[l[i]] = 0;
        }
        if(dict[l[i]]++ < n) {
            limited.push_back(l[i]);
        }
    }
    return limited;
}

int pop_growth(double p0, double g, double n, double p) {
    double v = p0;
    int i = 0;
    while(v < p){
        v = v * (1 + (g / 100.0f)) + n;
        i++;
    }
    return i;
}

bool is_triangle(double a, double b, double c) {
    if(a <= 0 || b <= 0 || c <= 0) { return false; }
    if(a + b <= c || b + c <= a || c + a <= b) { return false; }
    return true;
}
