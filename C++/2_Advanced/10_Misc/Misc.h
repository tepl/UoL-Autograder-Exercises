#ifndef Q10_H
#define Q10_H

#include <string>
#include <vector>

using namespace std;

// DO NOT MODIFY THIS FILE!
// THIS FILE DEFINES THE STRUCTURE OF THE FUNCTIONS WHICH WILL BE TESTED
// IF YOU MODIFY THE STRUCUTRE OF THE FUNCTION, YOUR RESULTS WILL NOT BE VALID


// API: Given an array of numbers l, sort ascending odd numbers, but even numbers must be on their places.
//      Zero is even. If l is empty, return an empty vector
// Params:
//  l - a list of whole numbers
// Returns:
//  list of numbers with odd numbers sorted ascending
// Example:
// s - {1, 9, 6, -5, 0, 8, 11, 1, 2}, return - {-5, 1, 6, 1, 0, 8, 9, 11, 2}
// l - {}, return - {}
vector<int> selective_sort(vector<int> l);

// API: Given a list l and a number N, create a new list that contains each number of l at most N times without reordering.
//  If l is empty, return an empty vector
// Params:
//  l - a list of numbers
//  n - the separator
// Returns:
//  the limited list
// Example:
// l - {1,2,3,1,2,1,2,3}, n - 2, return - {1,2,3,1,2,3}
// l - {1,1,1,1}, n - 2, return - {1,1}
// l - {20,37,20,21}, n - 1, return - {20,37,21}
vector<int> delete_nth(vector<int> l, int n);

// API: given an initial population p0, which grows by g percent every then increases by n every cycle, calculate how many cycles it takes to reach a population of p.
//  If p cannot be reached, return -1
// Params:
//  p0 - initial population
//  g - percentage population growth
//  n - population increase
//  p - target population
// Returns:
//  number of cycles until the population is greater than the target population
// Example:
// p0 - 100, g - 10, n - 5, p - 200, return - 6
int pop_growth(double p0, double g, double n, double p);

// API: given three numbers a, b, c return true if a valid triangle can be created with sides of length a, b, c
//  A triangle must have area greater than 0 to be valid
// Params:
//  a, b, c - numbers
// Returns:
//  True if a triangle can be created with a, b, c sides
// Example:
// a - 5, b - 4, c - 3, return - true
// a - 10, b - 3, c - 5, return - false
bool is_triangle(double a, double b, double c);

#endif // Q10_H