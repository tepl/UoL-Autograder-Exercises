#include "Misc.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

class DeleteNthEvaluator : public Evaluator<vector<int>>{
  public:
    vector<vector<int>> inputs {
      {1,2,3,1,2,1,2,3},
      {1,1,1,1},
      {20,37,20,21},
      {},
      {1, 2, 3, 1, 2, 1, 2, 3},
      {1,1,1,1},
      {33, 19, 19, 6, 6, 24, 6, 19, 6, 1, 19, 6},
      {37, 76, 37, 94, 37, 37},
      {12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12},
      {80, 26, 89, 89, 91, 89, 91, 19, 91, 26, 89, 91, 91, 19, 26, 91, 89, 19, 26},
    };
    
    vector<int> input_ns{
      2, 2, 1, 1, 0, -2, 1, 4, 1, 5
    };

    vector<vector<int>> expected_outputs {
      {1,2,3,1,2,3},
      {1,1},
      {20,37,21},
      {},
      {},
      {},
      {33, 19, 6, 24, 1},
      {37, 76, 37, 94, 37, 37},
      {12},
      {80, 26, 89, 89, 91, 89, 91, 19, 91, 26, 89, 91, 91, 19, 26, 89, 19, 26},
    };

    DeleteNthEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= (int)inputs.size()) { return ""; }
      return "delete_nth(" + vector_to_string(inputs[i]) + ", " + to_string(input_ns[i]) + ")";
    }

    vector<int> GetResult(int i){
      return delete_nth(inputs[i], input_ns[i]);
    }

    float GetScore(int i, vector<int> result){
      return result == expected_outputs[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, vector<int> result, float score){
      return "Returned " + vector_to_string(result) + ", expecting " + vector_to_string(expected_outputs[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  DeleteNthEvaluator evaluator(argc, argv);
  return evaluator.Run();
}