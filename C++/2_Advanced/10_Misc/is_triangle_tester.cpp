#include "Misc.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

class IsTriangleEvaluator : public Evaluator<bool>{
  public:
    vector<vector<int>> inputs {
      {5, 4, 3},
      {10, 3, 5},
      {-1, 3, 2},
      {3, -1, 2},
      {3, 2, -1},
      {1, 1, 1},
      {1000, 1000, 1},
      {1000, 1, 1},
      {0, 1, 1},
      {1, 0, 1},
      {1, 1, 0},
      {-1, -2, -2},
      {13, 14, 10},
      {1000, 501, 500}
    };
    
    vector<bool> expected_outputs {
      true, false, false, false, false, true, true, false, false, false, false, false, true, true
    };

    IsTriangleEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= (int)inputs.size()) { return ""; }
      return "is_triangle(" + to_string(inputs[i][0]) + ", " + to_string(inputs[i][1]) + ", " + to_string(inputs[i][2]) + ")";
    }

    bool GetResult(int i){
      return is_triangle(inputs[i][0], inputs[i][1], inputs[i][2]);
    }

    float GetScore(int i, bool result){
      return result == expected_outputs[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, bool result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(expected_outputs[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  IsTriangleEvaluator evaluator(argc, argv);
  return evaluator.Run();
}