#include "Misc.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

class SelectiveSortEvaluator : public Evaluator<vector<int>>{
  public:
    vector<vector<int>> inputs {
      {1, 9, 6, -5, 0, 8, 11, 1, 2},
      {},
      {90},
      {1, 3, 5, 7, 9, 11},
      {0, 2, 4, 6, 8, 10, 12},
      {11, 9, 7, 5, 3, 1, -1, -3},
      {1, 2, 3, 2, 1, 2, 3, 2, 1, 2, 3, 2},
      {-12, 34, -3, 66, 65, 68, -58, 16, -2, -18, -20, 31, -65, -37, 36},
      {-64, -56, 72, -44, -9, 85, -18},
      {82, 58, -20, -99, 53, 61, 71, 45, -21, 1, 46}
    };

    vector<vector<int>> expected_outputs {
      {-5, 1, 6, 1, 0, 8, 9, 11, 2},
      {},
      {90},
      {1, 3, 5, 7, 9, 11},
      {0, 2, 4, 6, 8, 10, 12},
      {-3, -1, 1, 3, 5, 7, 9, 11},
      {1, 2, 1, 2, 1, 2, 3, 2, 3, 2, 3, 2},
      {-12, 34, -65, 66, -37, 68, -58, 16, -2, -18, -20, -3, 31, 65, 36},
      {-64, -56, 72, -44, -9, 85, -18},
      {82, 58, -20, -99, -21, 1, 45, 53, 61, 71, 46}
    };

    SelectiveSortEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= (int)inputs.size()) { return ""; }
      return "selective_sort(" + vector_to_string(inputs[i]) + ")";
    }

    vector<int> GetResult(int i){
      return selective_sort(inputs[i]);
    }

    float GetScore(int i, vector<int> result){
      return result == expected_outputs[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, vector<int> result, float score){
      return "Returned " + vector_to_string(result) + ", expecting " + vector_to_string(expected_outputs[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  SelectiveSortEvaluator evaluator(argc, argv);
  return evaluator.Run();
}