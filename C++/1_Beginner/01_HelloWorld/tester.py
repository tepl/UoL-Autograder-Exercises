import py_eval_util as eval_util

def test_file():

    def score_provider(i, result):
        max_score = max(eval_util.levenshtein_ratio(str(r), "Hello World!") for r in result)
        if len(result) > 1:
            max_score -= 0.5
        return max(max_score, 0)

    def feedback_provider(i, result, score):
        max_score = max(eval_util.levenshtein_ratio(str(r), "Hello World!") for r in result)
        if max_score < 1:
            feedback = 'You printed {}, but "Hello World!" was expected'.format(', '.join(f'"{r}"' for r in result.split('\n')))
        else:
            feedback = f'You printed "Hello World!" correctly.'
        if len(result) > 1:
            feedback += " You've printed more than one line, so 0.5 marks were deducted"
        return feedback

    e = eval_util.Evaluator()
    e.with_name(lambda i: "Hello world" if i <= 0 else None)
    e.run_executable()
    e.with_score(score_provider)
    e.with_feedback(feedback_provider)
    e.start()

if __name__ == "__main__":
    test_file()