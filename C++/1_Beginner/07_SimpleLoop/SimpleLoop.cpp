#include <iostream>
#include <string>
#include <cstdio>
#include <cmath>

// Take a whole positive number passed in by the user
// Print the sum of all number between 0 and this number (including the number passed in)


int main()
{
    int a;
    int s = 0;
    std::cin >> a;
    while(a > 0){ s += a--; }
    std::cout << s;
}   