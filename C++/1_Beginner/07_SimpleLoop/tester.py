from py_eval_util import Evaluator

def test_file():
    test_cases = [
        1, 5, 100, 0
    ]
    expected = [
        str(sum(range(test_case + 1))) for test_case in test_cases
    ]

    e = Evaluator()
    e.with_name(
        lambda i: f"Simple loop: {test_cases[i]}" if i < len(test_cases) else None)
    e.run_executable()
    e.with_input(lambda i: test_cases[i])
    e.with_score(lambda i, result: result[0] == expected[i])
    e.with_feedback(
        lambda i, result, score: "Output: {}, Expected: {} : {}".format(', '.join(result), expected[i], "PASS" if score >= 1 else "FAIL"))
    e.start()

if __name__ == "__main__":
    test_file()