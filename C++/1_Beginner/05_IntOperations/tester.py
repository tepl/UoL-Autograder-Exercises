from py_eval_util import Evaluator

from math import ceil, floor

def cpp_int_div(a, b):
    return floor(a / b) if a / b > 0 else ceil(a / b)

def test_file():
    test_cases = [
        (-7, 4, -5, -9),
        (5, 7, 5, 5),
        (0, 9, -6, 10),
        (-7, 9, 7, 9),
        (-1, -10, -6, -3),
    ]
    test_funcs = [
        (lambda a, b, c, d: a + b),
        (lambda a, b, c, d: b - a),
        (lambda a, b, c, d: c - (a * b)),
        (lambda a, b, c, d: "NaN" if (c + a) == 0 else cpp_int_div(d, (c + a)))
    ]
    expected = [[str(f(*test_case)) for f in test_funcs] for test_case in test_cases]

    e = Evaluator()
    e.with_name(
        lambda i: f"Int operations: {test_cases[i]}" if i < len(test_cases) else None)
    e.run_executable()
    e.with_input(lambda i: test_cases[i])
    e.with_score(lambda i, result: result == expected[i])
    e.with_feedback(
        lambda i, result, score: "Output: '{}', Expected: '{}' : {}".format(', '.join(result), ', '.join(expected[i]), "PASS" if score >= 1 else "FAIL"))
    e.start()


if __name__ == "__main__":
    test_file()