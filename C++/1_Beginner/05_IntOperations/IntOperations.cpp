#include <iostream>
#include <string>
#include <cstdio>

// Take 4 whole numbers passed in by the user, a, b, c and d
// Print to the console on separate lines the value of:
// a + b
// b - a
// c - a * b
// d / (c + a)  - If (c + a is 0, print the string "NaN")

int main()
{
    int a, b, c, d;
    std::cin >> a;
    std::cin >> b;
    std::cin >> c;
    std::cin >> d;
    std::cout << a + b << std::endl;
    std::cout << b - a << std::endl;
    std::cout << c - (a * b) << std::endl;
    if(c+a != 0){
        std::cout << d / (c + a) << std::endl;
    }
    else{
        std::cout << "NaN" << std::endl;
    }
}   