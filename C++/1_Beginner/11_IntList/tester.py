from py_eval_util import Evaluator

def test_file():
    test_cases = [
        (3, 2, 5, 4, 6),
        (0, 2),
        (10, 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
        (5, 1, 1, 2, 3, 4, 5),
        (5, 3, 6, 4, 5, 2, 12),
    ]
    expected = [
        [str(n) for n in test_case[2:] if n % test_case[1] == 0] for test_case in test_cases
    ]
    
    e = Evaluator()
    e.with_name(
        lambda i: f"Int List: {test_cases[i]}" if i < len(test_cases) else None)
    e.run_executable()
    e.with_input(lambda i: test_cases[i])
    e.with_score(lambda i, result: result == expected[i])
    e.with_feedback(
        lambda i, result, score: "Output: '{}', Expected: '{}' : {}".format(', '.join(result), ', '.join(expected[i]), "PASS" if score >= 1 else "FAIL"))
    e.start()

if __name__ == "__main__":
    test_file()