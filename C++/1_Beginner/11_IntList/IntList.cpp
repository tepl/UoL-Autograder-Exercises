#include <iostream>
#include <string>
#include <cstdio>
#include <cmath>
#include <vector>

// Read in two positive whole numbers from the user, x and y in this order. 
// Next, read in x whole numbers, than print those, which are divisible by y


int main()
{
    int x, y;
    std::cin >> x;
    std::cin >> y;
    
    std::vector<int> nums;
    for(int i = 0; i < x; i++){
        int a;
        std::cin >> a;
        nums.push_back(a);
    }
    for(int i = 0; i < x; i++){
        if(nums[i] % y == 0){
            std::cout << nums[i] << std::endl;
        }
    }
}