#include <iostream>
#include <string>
#include <cstdio>
#include <cmath>

// Take 4 numbers passed in by the user, a, b, c and d
// Print to the console on separate lines the value of:
// higher value out of a and b
// a ^ c - if a <= 0 the string "NaN"
// the square root of d - if d < 0 the string "NaN"
// sin(b)

int main()
{
    float a, b, c, d;
    std::cin >> a;
    std::cin >> b;
    std::cin >> c;
    std::cin >> d;
    std::cout << fmax(a, b) << std::endl;
    if(a > 0)
        std::cout << pow(a, c) << std::endl;
    else
        std::cout << "NaN" << std::endl;
    if(d > 0)
        std::cout << sqrt(d) << std::endl;
    else
        std::cout << "NaN" << std::endl;
    std::cout << sin(b) << std::endl;
    
}   