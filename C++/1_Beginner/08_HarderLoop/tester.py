from py_eval_util import Evaluator

def test_file():
    test_cases = [
        (0, 10),
        (10, 20),
        (20, 10),
        (10, 11),
        (-10, -20),
        (-10, 10)
    ]
    expected = [
        str(sum(range(min(test_case) + 1, max(test_case)))) for test_case in test_cases
    ]

    e = Evaluator()
    e.with_name(
        lambda i: f"Harder loop: {test_cases[i]}" if i < len(test_cases) else None)
    e.run_executable()
    e.with_input(lambda i: test_cases[i])
    e.with_score(lambda i, result: result[0] == expected[i])
    e.with_feedback(
        lambda i, result, score: "Output: {}, Expected: {} : {}".format(', '.join(result), expected[i], "PASS" if score >= 1 else "FAIL"))
    e.start()

if __name__ == "__main__":
    test_file()