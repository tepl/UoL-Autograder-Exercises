#include <iostream>
#include <string>
#include <cstdio>
#include <algorithm>

// Take two whole numbers passed in by the user
// Print the sum of all number between the two numbers (not including the numbers passed in)


int main()
{
    int a, b;
    int s = 0;
    std::cin >> a;
    std::cin >> b;

    int start = std::min(a, b);
    int end = std::max(a, b);
    int i = start + 1;
    while(i < end){
        s += i++;
    }
    std::cout << s;
}   