#include <iostream>
#include <string>
#include <cstdio>
#include <string>
#include <vector>

// Read in a positive whole number x and a character c from the user in this order.
// Next, read in x strings, than print those, which start with c


int main()
{
    int x;
    char c;
    std::cin >> x;
    std::cin >> c;
    
    std::vector<std::string> words;
    for(int i = 0; i < x; i++){
        std::string a;
        std::cin >> a;
        words.push_back(a);
    }
    for(int i = 0; i < x; i++){
        if(words[i][0] == c){
            std::cout << words[i] << std::endl;
        }
    }
}