from py_eval_util import Evaluator

def test_file():
    test_cases = [
        (3, "f", "foo", "bar", "door"),
        (0, "x"),
        (10, "x", "foo", "foo", "foo", "foo", "foo", "foo", "foo", "foo", "foo", "foo"),
        (5, "E", "ELEC1620", "ELEC2645", "MECH1206", "COMP2611", "ELEC3662"),
        (5, "?", "?!!", "??!", "!??", "!?!", "???"),
    ]
    expected = [
        [str(n) for n in test_case[2:] if n[0] == test_case[1]] for test_case in test_cases
    ]
    e = Evaluator()
    e.with_name(
        lambda i: f"String list: {test_cases[i]}" if i < len(test_cases) else None)
    e.run_executable()
    e.with_input(lambda i: test_cases[i])
    e.with_score(lambda i, result: result == expected[i])
    e.with_feedback(
        lambda i, result, score: "Output: '{}', Expected: '{}' : {}".format(', '.join(result), ', '.join(expected[i]), "PASS" if score >= 1 else "FAIL"))
    e.start()

if __name__ == "__main__":
    test_file()