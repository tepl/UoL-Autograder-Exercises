#include <iostream>
#include <string>
#include <cstdio>

// Take 2 whole numbers passed in by the user, a and b
// Print on the console:
// "Equal" if a == b
// "A is greater" if a > b
// "B is greater" if b > a

int main()
{
    int a, b;
    std::cin >> a;
    std::cin >> b;
    if(a == b){
        printf("Equal");
    }
    else if(a > b){
        printf("A is greater");
    }
    else{
        printf("B is greater");
    }
}