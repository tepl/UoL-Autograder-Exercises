from py_eval_util import Evaluator

def test_file():
    test_cases = [
        ((0, 0), "Equal"),
        ((1, 0), "A is greater"),
        ((0, 1), "B is greater"),
        ((-2, -2), "Equal"),
        ((-1, -10), "A is greater"),
        ((-3, 0), "B is greater"),
        ((100, 100), "Equal"),
        ((100, 99), "A is greater"),
        ((-100, 100), "B is greater"),
    ]

    def score_provider(i, result):
        try:
            return result[0] == test_cases[i][1]
        except:
            return 0        

    e = Evaluator()
    e.with_name(
        lambda i: f"Simple branching: {test_cases[i][0]}" if i < len(test_cases) else None)
    e.run_executable()
    e.with_input(lambda i: test_cases[i][0])
    e.with_score(score_provider)
    e.with_feedback(
        lambda i, result, score: "Output: '{}', Expected: '{}' : {}".format(', '.join(result), test_cases[i][1], "PASS" if score >= 1 else "FAIL"))
    e.start()


if __name__ == "__main__":
    test_file()