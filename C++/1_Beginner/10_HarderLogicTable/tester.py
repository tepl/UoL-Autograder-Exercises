from py_eval_util import Evaluator

def test_file():
    test_cases = [
        ((0, 0, 0), 0),
        ((0, 0, 1), 1),
        ((0, 1, 0), 0),
        ((0, 1, 1), 1),
        ((1, 0, 0), 0),
        ((1, 0, 1), 1),
        ((1, 1, 0), 1),
        ((1, 1, 1), 0),
        ((2, 3, 4), "Invalid input"),
        (("Hello", "world", "!"), "Invalid input"),
    ]
    
    e = Evaluator()
    e.with_name(
        lambda i: f"Harder logic table: {test_cases[i][0]}" if i < len(test_cases) else None)
    e.run_executable()
    e.with_input(lambda i: test_cases[i][0])
    e.with_score(lambda i, result: result[0] == str(test_cases[i][1]))
    e.with_feedback(
        lambda i, result, score: "Output: {}, Expected: {} : {}".format(', '.join(result), test_cases[i][1], "PASS" if score >= 1 else "FAIL"))
    e.start()

if __name__ == "__main__":
    test_file()