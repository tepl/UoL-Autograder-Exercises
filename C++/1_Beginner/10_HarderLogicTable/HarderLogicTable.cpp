#include <iostream>
#include <string>
#include <cstdio>
#include <cmath>

// Take three whole numbers from the user, a, b and c. All of them has to be 0 or 1. If they're not, print "Invalid input"
// Implement the following logic table, and output O:
/*
| a | b | c | O |
|---|---|---|---|
| 0 | 0 | 0 | 0 |
| 0 | 0 | 1 | 1 |
| 0 | 1 | 0 | 0 |
| 0 | 1 | 1 | 1 |
| 1 | 0 | 0 | 0 |
| 1 | 0 | 1 | 1 |
| 1 | 1 | 0 | 1 |
| 1 | 1 | 1 | 0 |
*/


int main()
{
    int a, b, c;
    std::cin >> a;
    std::cin >> b;
    std::cin >> c;
    if(!(a == 0 || a == 1) || !(b == 1 || b == 0) || !(c == 1 || c == 0)){
        std::cout << "Invalid input";
    }
    else{
        bool _a = a == 1, _b = b == 1, _c = c == 1;
        std::cout << (((_a && _b && !_c) || (_c && !(_a && _b))) ? 1 : 0);
    }
}