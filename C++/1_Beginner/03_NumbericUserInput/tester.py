from py_eval_util import Evaluator

def test_file():
    test_cases = [
        (0, 3),
        (3, 6),
        (-3, 0),
        (-6, -3),
    ]

    def score_provider(i, result):
        try:
            res = int(result[0])
            return res == test_cases[i][1]
        except:
            return 0        

    e = Evaluator()
    e.with_name(
        lambda i: f"Numeric user input: {test_cases[i][0]}" if i < len(test_cases) else None)
    e.run_executable()
    e.with_input(lambda i: test_cases[i][0])
    e.with_score(score_provider)
    e.with_feedback(
        lambda i, result, score: "Output: {}, Expected: {} : {}".format(', '.join(result), test_cases[i][1], "PASS" if score >= 1 else "FAIL"))
    e.start()

if __name__ == "__main__":
    test_file()