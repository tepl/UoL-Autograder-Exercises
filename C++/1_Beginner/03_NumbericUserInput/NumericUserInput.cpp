#include <iostream>
#include <string>
#include <cstdio>

// Take a whole number passed in by the user, add 3 to the number, and print it on the console.

int main()
{
    int a;
    std::cin >> a;
    std::cout << a + 3;
}