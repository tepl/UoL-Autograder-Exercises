import py_eval_util as eval_util

def test_file():
    test_cases = [
        "foo",
        "bar",
        "Hello World",
        "a" * 254
    ]

    e = eval_util.Evaluator()
    e.with_name(
        lambda i: f"Basic input: {test_cases[i]}" if i < len(test_cases) else None)
    e.run_executable()
    e.with_input(
        lambda i: test_cases[i])
    e.with_score(
        lambda i, result: result[0] == test_cases[i])
    e.with_feedback(
        lambda i, result, score: "Output: '{}', Expected: '{}' : {}".format(', '.join(result), test_cases[i], "PASS" if score >= 1 else "FAIL"))
    e.start()

if __name__ == "__main__":
    test_file()