// YOU MAY MODIFY THIS FILE, AS LONG AS YOU DON'T MODIFY THE API DEFINED IN THE .H FILE
#include <iostream>
#include <cstdio>
#include "Queue.h"

using namespace std;

int main() {
    Queue q;
    q.Enqueue(10);
    q.Enqueue(20);
    q.Enqueue(100);
    long result = q.Dequeue();
    printf("Dequeued the following item: %d\n", result);
}
