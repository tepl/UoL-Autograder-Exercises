#include "Queue.h"

Queue::Queue() : _head(nullptr), _tail(nullptr){}

Queue::~Queue(){
    while(_head != nullptr){
        LinkedItem* temp = _head;
        _head = _head->next;
        delete temp;
    }
}

void Queue::Enqueue(long v){
    LinkedItem *new_item = new LinkedItem;
    new_item->value = v;
    new_item->next = nullptr;
    if(_head == nullptr || _tail == nullptr){
        _head = new_item;
        _tail = new_item;
        return;
    }
    _tail->next = new_item;
    _tail = new_item;
}

long Queue::Dequeue(){
    if(_head == nullptr){
        return -1;
    }
    LinkedItem *temp = _head;
    long val = _head->value;
    _head = _head->next;
    delete temp;
    return val;
}