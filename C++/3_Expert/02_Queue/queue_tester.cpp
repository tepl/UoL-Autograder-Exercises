#include "cpp_eval_util.h"
#include <string>
#include "Queue.h"

using namespace std;

#define TEST_CASE_COUNT 10

long case1(){
    Queue q;
    q.Enqueue(10);
    q.Enqueue(20);
    q.Enqueue(100);
    return q.Dequeue();
}
long case2(){
    Queue q;
    q.Enqueue(14);
    return q.Dequeue();
}
long case3(){
    Queue q;
    q.Enqueue(10);
    q.Dequeue();
    return q.Dequeue();
}
long case4(){
    Queue q;
    q.Enqueue(8641);
    q.Enqueue(31415);
    q.Dequeue();
    q.Enqueue(16);
    q.Enqueue(1);
    q.Dequeue();
    q.Enqueue(36156);
    q.Enqueue(16815);
    q.Dequeue();
    q.Dequeue();
    q.Dequeue();
    q.Enqueue(-85416);
    q.Enqueue(9841);
    q.Enqueue(314);
    q.Enqueue(5865);
    q.Dequeue();
    q.Enqueue(52);
    q.Enqueue(24);
    q.Dequeue();
    q.Dequeue();
    return q.Dequeue();
}
long case5(){
    Queue q;
    q.Enqueue(8641);
    q.Enqueue(31415);
    q.Dequeue();
    q.Dequeue();
    q.Dequeue();
    q.Enqueue(16);
    q.Enqueue(1);
    q.Dequeue();
    q.Enqueue(36156);
    q.Enqueue(16815);
    q.Dequeue();
    q.Dequeue();
    q.Dequeue();
    q.Dequeue();
    q.Dequeue();
    q.Dequeue();
    return q.Dequeue();
}
long case6(){
    Queue q;
    for(int i = 0; i < 1e4; i++){
        q.Dequeue();
    }
    return q.Dequeue();
}
long case7(){
    Queue q;
    for(int i = 0; i < 1e6; i++){
        q.Enqueue(i);
    }
    q.Enqueue(1000000);
    return q.Dequeue();
}
long case8(){
    Queue q;
    for(int i = 0; i < 90000000; i++){   // This should exhaust the allocated 256MB limit
        q.Enqueue(10);
        q.Dequeue();
    }
    q.Enqueue(10);
    return q.Dequeue();
}

long case9(){
    Queue* q = new Queue();
    q->Enqueue(10);
    delete q;
    return 0;
}

long case10(){
    for(int i = 0; i < 1000000; i++){
        Queue* q = new Queue();
        for(int j = 0; j < i % 100; j++){
            q->Enqueue(j);
        }
        delete q;
    }
    return 0;
}

class QueueEvaluator : public Evaluator<long>{
  public:
    string test_names[TEST_CASE_COUNT] = {
        "Queue: Enqueue 3 Dequeue 1",
        "Queue: Enqueue 1 Dequeue 1",
        "Queue: Enqueue 1 Dequeue 2",
        "Queue: Enqueue Dequeue mix 1",
        "Queue: Enqueue Dequeue mix 2",
        "Queue: Dequeue many",
        "Queue: Enqueue many Dequeue 1",
        "Queue: Enqueue Dequeue many",
        "Queue: create destroy object",
        "Queue: create destroy object many"
    };

    long expected[TEST_CASE_COUNT] = {
        10, 14, -1, 314, -1, -1, 0, 10
    };

    long (*functptr[TEST_CASE_COUNT])() = { case1, case2, case3, case4, case5, case6, case7, case8, case9, case10 } ;

    QueueEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      return i < TEST_CASE_COUNT ? test_names[i] : "";
    }

    long GetResult(int i){
      return (*functptr[i])();
    }

    float GetScore(int i, long result){
      return result == expected[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, long result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(expected[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  QueueEvaluator evaluator(argc, argv);
  return evaluator.Run();
}