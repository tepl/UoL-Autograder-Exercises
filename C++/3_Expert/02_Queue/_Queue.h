#ifndef Q2_H
#define Q2_H

using namespace std;

// API: 
//  Implement the following Queue class
//  A Queue is a First in-First out data structure.
//  When Enqueue is called, an item is passed it, which becomes the last item
//  When Dequeue is called, the first item is returned and the item enqueued after this item becomes the first item
//  If the queue is empty and enqueue is called, it should return -1
//  The queue has no size limit
// Example:
//  Queue q;
//  q.Enqueue(10);
//  q.Enqueue(20);
//  q.Enqueue(100);
//  q.Dequeue(); = 10

// DO NOT MODIFY THE PUBLIC MEMBERS THIS CLASS!
// THIS CLASS DEFINES THE STRUCTURE OF WHAT WILL BE TESTED
// IF YOU MODIFY THE PUBLIC STRUCUTRE OF THE CLASS, YOUR RESULTS WILL NOT BE VALID
class Queue{
    public:
        Queue();
        ~Queue();
        void Enqueue(long v);
        long Dequeue();
    private:
        
};

#endif // Q2_H