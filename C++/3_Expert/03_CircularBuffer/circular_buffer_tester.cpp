#include "cpp_eval_util.h"
#include <string>
#include "CircularBuffer.h"

using namespace std;

#define TEST_CASE_COUNT 15

long case1(){
    CircularBuffer b(10);
    b.Enqueue(10);
    b.Enqueue(20);
    b.Enqueue(100);
    return b.Dequeue();
}
long case2(){
    CircularBuffer b(2);
    b.Enqueue(10);
    b.Enqueue(20);
    b.Enqueue(100);
    return b.Dequeue();
}
long case3(){
    CircularBuffer b(10);
    b.Enqueue(14);
    return b.Dequeue();
}
long case4(){
    CircularBuffer b(10);
    b.Enqueue(10);
    b.Dequeue();
    return b.Dequeue();
}
long case5(){
    CircularBuffer b(10);
    b.Enqueue(8641);
    b.Enqueue(31415);
    b.Dequeue();
    b.Enqueue(16);
    b.Enqueue(1);
    b.Dequeue();
    b.Enqueue(36156);
    b.Enqueue(16815);
    b.Dequeue();
    b.Dequeue();
    b.Dequeue();
    b.Enqueue(-85416);
    b.Enqueue(9841);
    b.Enqueue(314);
    b.Enqueue(5865);
    b.Dequeue();
    b.Enqueue(52);
    b.Enqueue(24);
    b.Dequeue();
    b.Dequeue();
    return b.Dequeue();
}
long case6(){
    CircularBuffer b(10);
    b.Enqueue(8641);
    b.Enqueue(31415);
    b.Dequeue();
    b.Dequeue();
    b.Dequeue();
    b.Enqueue(16);
    b.Enqueue(1);
    b.Dequeue();
    b.Enqueue(36156);
    b.Enqueue(16815);
    b.Dequeue();
    b.Dequeue();
    b.Dequeue();
    b.Dequeue();
    b.Dequeue();
    b.Dequeue();
    return b.Dequeue();
}
long case7(){
    CircularBuffer b(10);
    b.Enqueue(8641);
    b.Enqueue(31415);
    b.Enqueue(31415);
    b.Enqueue(31415);
    b.Enqueue(31415);
    b.Enqueue(31415);
    b.Dequeue();
    b.Dequeue();
    b.Dequeue();
    b.Enqueue(16);
    for(int i = 0; i < 10; i++){
        b.Enqueue(i);
    }
    b.Dequeue();
    b.Dequeue();
    b.Dequeue();
    b.Dequeue();
    b.Dequeue();
    b.Dequeue();
    b.Dequeue();
    return b.Dequeue();
}
long case8(){
    CircularBuffer b(10);
    for(int i = 0; i < 20; i++){
        b.Enqueue(i);
    }
    for(int i = 0; i < 15; i++){
        b.Dequeue();
    }
    for(int i = 0; i < 12; i++){
        b.Enqueue(i * 10);
    }
    return b.Dequeue();
}
long case9(){
    CircularBuffer b(10);
    for(int i = 0; i < 1e4; i++){
        b.Dequeue();
    }
    return b.Dequeue();
}
long case10(){
    CircularBuffer b(10000);
    for(int i = 0; i < 1e8; i++){
        b.Enqueue(i);
    }
    b.Enqueue(1000000);
    return b.Dequeue();
}
long case11(){
    CircularBuffer b(10000);
    for(int i = 0; i < 90000000; i++){   // This should exhaust the allocated 256MB limit if memory allocation is done incorrectly
        b.Enqueue(10);
        b.Dequeue();
    }
    b.Enqueue(10);
    return b.Dequeue();
}
long case12(){
    CircularBuffer b(0);
    return b.Dequeue();
}
long case13(){
    CircularBuffer b(1);
    b.Enqueue(-2);
    return b.Dequeue();
}

long case14(){
    CircularBuffer* b = new CircularBuffer(10);
    b->Enqueue(10);
    delete b;
    return 0;
}

long case15(){
    for(int i = 0; i < 1000000; i++){
        CircularBuffer* b = new CircularBuffer(i % 80);
        for(int j = 0; j < i % 100; j++){
            b->Enqueue(j);
        }
        delete b;
    }
    return 0;
}

class QueueEvaluator : public Evaluator<long>{
  public:
    string test_names[TEST_CASE_COUNT] = {
        "CircularBuffer(10): Enqueue 3 Dequeue 1",
        "CircularBuffer(2): Enqueue 3 Dequeue 1",
        "CircularBuffer(10): Enqueue 1 Dequeue 1",
        "CircularBuffer(10): Enqueue 1 Dequeue 2",
        "CircularBuffer(10): Enqueue Dequeue mix 1",

        "CircularBuffer(10): Enqueue Dequeue mix 2",
        "CircularBuffer(10): Enqueue Dequeue mix 3",
        "CircularBuffer(10): Enqueue Dequeue mix 4",
        "CircularBuffer(10): Dequeue many",
        "CircularBuffer(10000): Enqueue many Dequeue 1",

        "CircularBuffer(10000): Enqueue Dequeue many",
        "CircularBuffer(0): Dequeue",
        "CircularBuffer(1): Enqueue Dequeue",
        "CircularBuffer(10): Create destroy",
        "CircularBuffer(var): Create destroy many"
    };

    long expected[TEST_CASE_COUNT] = {
        10, 20, 14, -1, 314,
        -1, 7, 20, -1, 99990001,
        10, -1, -2, 0, 0
    };

    long (*functptr[TEST_CASE_COUNT])() = { case1, case2, case3, case4, case5, case6, case7, case8, case9, case10, case11, case12, case13, case14, case15 } ;

    QueueEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      return i < TEST_CASE_COUNT ? test_names[i] : "";
    }

    long GetResult(int i){
      return (*functptr[i])();
    }

    float GetScore(int i, long result){
      return result == expected[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, long result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(expected[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  QueueEvaluator evaluator(argc, argv);
  return evaluator.Run();
}