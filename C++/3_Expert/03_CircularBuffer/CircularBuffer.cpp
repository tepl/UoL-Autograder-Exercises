#include "CircularBuffer.h"

CircularBuffer::CircularBuffer(int capacity) : _buffer(new long[capacity]), _capacity(capacity), _head(0), _tail(0), _full(false) {
}

CircularBuffer::~CircularBuffer(){
    delete[] _buffer;
}

void CircularBuffer::Enqueue(long v){
    if(_capacity <= 0){
        return;
    }
    _buffer[_head] = v;
    if(_full){
        _tail = (_tail + 1) % _capacity;
    }
    _head = (_head + 1) % _capacity;
    _full = _head == _tail;
}

long CircularBuffer::Dequeue(){
    if(_capacity <= 0 || (!_full && _head == _tail)){
        return -1;
    }
    long val = _buffer[_tail];
    _full = false;
    _tail = (_tail + 1) % _capacity;
    return val;
}