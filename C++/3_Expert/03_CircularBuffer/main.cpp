// YOU MAY MODIFY THIS FILE, AS LONG AS YOU DON'T MODIFY THE API DEFINED IN THE .H FILE
#include <iostream>
#include <cstdio>
#include "CircularBuffer.h"

using namespace std;

int main() {
    CircularBuffer b(10);
    b.Enqueue(10);
    b.Enqueue(20);
    b.Enqueue(100);
    long result = b.Dequeue();
    printf("Dequeued the following item: %d\n", result);
}