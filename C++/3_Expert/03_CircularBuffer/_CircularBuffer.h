#ifndef Q3_H
#define Q3_H

using namespace std;

// API: 
//  Implement the following CircularBuffer class
//  A CircularBuffer is a First in-First out data structure with a limited capacity.
//  When Enqueue is called, an item is passed it, which becomes the last item
//  When Dequeue is called, the first item is returned and the item enqueued after this item becomes the first item
//  If the buffer is empty and enqueue is called, it should return -1
//  The capacity of the buffer is set by the capacity parameter of the constructor
//  If the capacity of the buffer is exceeded, the first item is discarded, and the second item becomes the first item
//  If the capacity is 0, Dequeue returns -1
// Example 1:
//  CircularBuffer b(10);
//  b.Enqueue(10);
//  b.Enqueue(20);
//  b.Enqueue(100);
//  b.Dequeue(); = 10
// Example 2:
//  CircularBuffer b(2);
//  b.Enqueue(10);
//  b.Enqueue(20);
//  b.Enqueue(100);
//  b.Dequeue(); = 20

// DO NOT MODIFY THE PUBLIC MEMBERS THIS CLASS!
// THIS CLASS DEFINES THE STRUCTURE OF WHAT WILL BE TESTED
// IF YOU MODIFY THE PUBLIC STRUCUTRE OF THE CLASS, YOUR RESULTS WILL NOT BE VALID
class CircularBuffer{
    public:
        CircularBuffer(int capacity);
        void Enqueue(long v);
        long Dequeue();
    private:
        
};

#endif // Q3_H