#ifndef Q1_H
#define Q1_H

using namespace std;

class LinkedItem{       // Don't override this .h file! Must upload .h file
    public:
        LinkedItem* next;
        long value;
};

// API: 
//  Implement the following Stack class
//  A Stack is a First in-Last out data structure.
//  When push is called, an item is passed it, which becomes the top item
//  When pop is called, the top item is returned and the item pushed before this item becomes the top item
//  If the stack is empty and pop is called, it should return -1
//  The stack has no size limit
// Example:
//  Stack s;
//  s.Push(10);
//  s.Push(20);
//  s.Push(100);
//  s.Pop(); = 100

// DO NOT MODIFY THE PUBLIC MEMBERS THIS CLASS!
// THIS CLASS DEFINES THE STRUCTURE OF WHAT WILL BE TESTED
// IF YOU MODIFY THE PUBLIC STRUCUTRE OF THE CLASS, YOUR RESULTS WILL NOT BE VALID
class Stack{
    public:
        Stack();
        ~Stack();
        void Push(long v);
        long Pop();
    private:
        LinkedItem* _head;
};

#endif // Q1_H