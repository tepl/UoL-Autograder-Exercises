// YOU MAY MODIFY THIS FILE, AS LONG AS YOU DON'T MODIFY THE API DEFINED IN THE .H FILE
#include <iostream>
#include <cstdio>
#include "Stack.h"

using namespace std;

int main() {
    Stack s;
    s.Push(10);
    s.Push(20);
    s.Push(100);
    long result = s.Pop();
    printf("Poped the following item: %d\n", result);
}
