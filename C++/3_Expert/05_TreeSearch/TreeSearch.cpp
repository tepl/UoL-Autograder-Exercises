#include "TreeSearch.h"

#include <queue>
#include <stack>
#include <string>

int Find_DepthFirst(Node *root, string value) {
  stack<Node *> search;
  search.push(root);

  while (!search.empty()) {
    Node *visited = search.top();
    search.pop();
    if (visited->value == value) {
      return visited->id;
    }
    if (visited->right != nullptr) {
      search.push(visited->right);
    }
    if (visited->left != nullptr) {
      search.push(visited->left);
    }
  }

  return -1;
}

int Find_BreadthFirst(Node *root, string value) {
  queue<Node *> search;
  search.push(root);

  while (!search.empty()) {
    Node *visited = search.front();
    search.pop();
    if (visited->value == value) {
      return visited->id;
    }
    if (visited->left != nullptr) {
      search.push(visited->left);
    }
    if (visited->right != nullptr) {
      search.push(visited->right);
    }
  }

  return -1;
}