#ifndef Q5_H
#define Q5_H

#include <string>

using namespace std;

// DO NOT MODIFY THIS CLASS!
// THIS IS PASSED IN TO YOUR FUNCTIONS BEING TESTED
// IF YOU MODIFY THE CLASS, YOUR RESULTS WILL NOT BE VALID
class Node {
 public:
  int id;
  string value;
  Node(int id, string value) : id(id), value(value) {}
  Node *left = nullptr;
  Node *right = nullptr;
};

/*
API:
 Implement the following Tree Search functions
 A binary tree is made up of nodes which contain some values. In this case an id
and a value. Each node can have two child nodes (left and right). The first node
is the root of the tree. All other nodes will be created as the child under this
root node. In this case, this tree is used to find certain data.

 In both functions in this exercise you're given the root of a binary tree and a
value to find in the tree. You have to return the id of the first node you find
with the given value.

 Depth first search:
    when searching depth first, you check the value of the current node, then
traverse the left node, then the right node (preorder tree traversal). this
results in finding values in the left side of the tree before finding values in
the right side. Breadth first search: when searching breadth first you start at
the root and explor all the neighbour nodes at the present depth prior to moving
on to the nodes at the next depth, starting with the child node on the left.
    this results in finding values that are closer to the root before finding
values further away from the root In both case, if you can't find the value in
the tree, return -1.

 Note: do not traverse the entire tree before returning a value. Return the id
as soon as you find a node with the required value.

Example:
 Node n1(1, "foo");
 Node n2(2, "bar");
 n1.left = &n2;
 Node n3(3, "baz");
 n1.right = &n3;
 Node n4(4, "baz");
 n2.left = &n4;
 Node n5(5, "qux");
 n3.left = &n5;
 Node n6(6, "dex");
 n3.right = &n6;

 The resulting tree will look like this:
         (1, "foo")
         //       \\
     (2, "bar")  (3, "baz")
       //        //       \\
   (4, "baz") (5, "qux") (6, "dex")

 Find_DepthFirst(&n1, "baz") == 4;
 Find_BreadthFirst(&n1, "baz") == 3;
*/

// DO NOT MODIFY THESE FUNCTIONS!
// THESE FUNCTIONS DEFINE THE STRUCTURE OF WHAT WILL BE TESTED
// IF YOU MODIFY THE FUNCTIONS, YOUR RESULTS WILL NOT BE VALID
int Find_DepthFirst(Node *root, string value);
int Find_BreadthFirst(Node *root, string value);

#endif  // Q5_H