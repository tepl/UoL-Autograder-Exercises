#include <string>

#include "TreeSearch.h"
#include "cpp_eval_util.h"

using namespace std;

#define TEST_CASE_COUNT 7

// Find_DepthFirst: Single node
int case1() {
  Node n1(1, "foo");
  return Find_DepthFirst(&n1, "foo");
}
// Find_DepthFirst: Multiple nodes
int case2() {
  Node n1(1, "foo");
  Node n2(2, "bar");
  n1.left = &n2;
  Node n3(3, "baz");
  n1.right = &n3;
  Node n4(4, "baz");
  n2.left = &n4;
  Node n5(5, "qux");
  n3.left = &n5;
  Node n6(6, "dex");
  n3.right = &n6;
  return Find_DepthFirst(&n1, "baz");
}
// Find_DepthFirst: Value not found
int case3() {
  Node n1(1, "foo");
  Node n2(2, "bar");
  n1.left = &n2;
  Node n3(3, "baz");
  n1.right = &n3;
  Node n4(4, "baz");
  n2.left = &n4;
  Node n5(5, "qux");
  n3.left = &n5;
  Node n6(6, "dex");
  n3.right = &n6;
  return Find_DepthFirst(&n1, "no");
}
// Find_DepthFirst: Value in node and elsewhere
int case4() {
  Node n1(1, "baz");
  Node n2(2, "bar");
  n1.left = &n2;
  Node n3(3, "baz");
  n1.right = &n3;
  Node n4(4, "baz");
  n2.left = &n4;
  Node n5(5, "qux");
  n3.left = &n5;
  Node n6(6, "dex");
  n3.right = &n6;
  return Find_DepthFirst(&n1, "baz");
}
// Find_DepthFirst: Deep left tree
int case5() {
  Node n1(1, "foo");
  int index = 1;
  Node* parent = &n1;
  for (int i = 0; i < 1000; i++) {
    Node* n = new Node(++index, "");
    parent->left = n;
    parent = n;
  }
  Node n2(index + 1, "bar");
  parent->left = &n2;
  return Find_DepthFirst(&n1, "bar");
}
// Find_DepthFirst: Infinite right tree
int case6() {
  Node n1(1, "foo");
  Node inf(0, "inf");
  inf.left = &inf;
  inf.right = &inf;
  n1.right = &inf;
  Node n2(2, "bar");
  n1.left = &n2;
  Node n3(3, "baz");
  n2.left = &n3;
  Node n4(4, "qux");
  n3.left = &n4;
  return Find_DepthFirst(&n1, "qux");
}
// Find_DepthFirst: Deep left tree, value on right
int case7() {
  Node n1(1, "foo");
  int index = 1;
  Node* parent = &n1;
  for (int i = 0; i < 100; i++) {
    Node* n = new Node(++index, "");
    parent->left = n;
    parent = n;
  }
  Node n2(index + 1, "bar");
  n1.right = &n2;
  return Find_DepthFirst(&n1, "bar");
}

class DepthFirstEvaluator : public Evaluator<int> {
 public:
  string test_names[TEST_CASE_COUNT] = {
      "Find_DepthFirst: Single node",
      "Find_DepthFirst: Multiple nodes",
      "Find_DepthFirst: Value not found",
      "Find_DepthFirst: Value in node and elsewhere",
      "Find_DepthFirst: Deep left tree",
      "Find_DepthFirst: Infinite right tree",
      "Find_DepthFirst: Deep left tree, value on right"};

  int expected[TEST_CASE_COUNT] = {1, 4, -1, 1, 1002, 4, 102};

  int (*functptr[TEST_CASE_COUNT])() = {case1, case2, case3, case4,
                                        case5, case6, case7};

  DepthFirstEvaluator(int argc, char** argv) : Evaluator(argc, argv) {}

  string GetName(int i) { return i < TEST_CASE_COUNT ? test_names[i] : ""; }

  int GetResult(int i) { return (*functptr[i])(); }

  float GetScore(int i, int result) {
    return result == expected[i] ? 1.0f : 0.0f;
  }

  string GetFeedback(int i, int result, float score) {
    return "Returned " + to_string(result) + ", expecting " +
           to_string(expected[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
  }
};

int main(int argc, char** argv) {
  DepthFirstEvaluator evaluator(argc, argv);
  return evaluator.Run();
}