// YOU MAY MODIFY THIS FILE, AS LONG AS YOU DON'T MODIFY THE API DEFINED IN THE .H FILE
#include <iostream>
#include <cstdio>
#include "string.h"
#include "TreeSearch.h"

using namespace std;

int main()
{
    Node n1(1, "foo");
    Node n2(2, "bar");
    n1.left = &n2;
    Node n3(3, "baz");
    n1.right = &n3;
    Node n4(4, "baz");
    n2.left = &n4;
    Node n5(5, "qux");
    n3.left = &n5;
    Node n6(6, "dex");
    n3.right = &n6;

    int df = Find_DepthFirst(&n1, "baz");
    printf("DepthFirst search result: %d\n", df);

    int bf = Find_BreadthFirst(&n1, "baz");
    printf("BreadthFirst search result: %d\n", bf);
}