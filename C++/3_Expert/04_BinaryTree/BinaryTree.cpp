#include "BinaryTree.h"
#include <string>

#include <fstream>
#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <cstdlib>

Node::Node(int id, string value) : value(value), id(id) {

}

Node::~Node(){
    
}

BinaryTree::BinaryTree() : _root(nullptr) {
}

BinaryTree::~BinaryTree(){
    vector<Node*> nodes = _TransverseInorder(_root);
    for(unsigned int i = 0; i < nodes.size(); i++){
        delete nodes[i];
    }
}

bool BinaryTree::Add(int id, string value){
    if(_root == nullptr){
        _root = new Node(id, value);
        return true;
    }
    else{
        return _insert(_root, id, value);
    }
}

bool BinaryTree::_insert(Node* current, int id, string value){
    if(id == current->id){
        return false;
    }
    else if(id < current->id){
        if(current->left == nullptr){
            current->left = new Node(id, value);
            current->left->parent = current;
            return true;
        }
        else{
            return _insert(current->left, id, value);
        }
    }
    else if(id > current->id){
        if(current->right == nullptr){
            current->right = new Node(id, value);
            current->right->parent = current;
            return true;
        }
        else{
            return _insert(current->right, id, value);
        }
    }
    return false;
}

Node* BinaryTree::_find(Node* current, int id){
    if(current == nullptr){
        return nullptr;
    }
    else if(id == current->id){
        return current;
    }
    else if(id < current->id){
        return _find(current->left, id);
    }
    else if(id > current->id){
        return _find(current->right, id);
    }
    return nullptr;
}

void BinaryTree::_print(Node* current, int indent){
    string output = "";
    for(unsigned int i = 0; i < indent; i++)
        output += "\t";
    
    if(current == nullptr){
        output += "nullptr";
        printf("%s\n", output.c_str());
    }
    else{
        output += "(" + to_string(current->id) + ", " + current->value + ")";
        printf("%s\n", output.c_str());
        _print(current->left, indent + 1);
        _print(current->right, indent + 1);
    }
}

bool BinaryTree::Remove(int v){
    // printf("Trying to delete: %d\n", v);
    // _print(_root, 0);
    Node* n = _find(_root, v);
    if(n == nullptr){
        return false;
    }
    else if(n->left == nullptr && n->right == nullptr){
        // printf("Found with id (1): %d\n", n->id);
        if(n->parent == nullptr){
            _root = nullptr;
        }
        else{
            if(n->parent->left == n){
                n->parent->left = nullptr;
            }
            else{
                n->parent->right = nullptr;
            }
        }
        delete n;
        // printf("After delete: \n");
        // _print(_root, 0);
        return true;
    }
    else if(n->left == nullptr || n->right == nullptr){
        // printf("Found with id (2): %d\n", n->id);
        Node* child = n->right == nullptr ? n->left : n->right;
        if(n->parent == nullptr){
            _root = child;
            child->parent = nullptr;
        }
        else{
            if(n->parent->left == n){
                n->parent->left = child;
                child->parent = n->parent;
            }
            else{
                n->parent->right = child;
                child->parent = n->parent;
            }
        }
        delete n;
        // printf("After delete: \n");
        // _print(_root, 0);
        return true;
    }
    else{
        // printf("Found with id (3): %d\n", n->id);
        vector<Node*> nodes = _TransverseInorder(_root);
        int match = -1;
        for(unsigned int i = 0; i < nodes.size(); i++){
            if(nodes[i] == n){
                match = i;
            }
        }
        if(match == -1){ return false; }
        Node* next = nodes[match + 1];
        int next_id = next->id;
        string next_value = next->value;
        Remove(next_id);
        n->id = next_id;
        n->value = next_value;
        // printf("After delete: \n");
        // _print(_root, 0);
        return true;
    }
}

string BinaryTree::Get(int v){
    Node* n = _find(_root, v);
    if(n == nullptr){
        return "";
    }
    return n->value;
}

vector<Node*> BinaryTree::_TransverseInorder(Node* current){
    vector<Node*> res;
    if(current == nullptr)
        return res;
    vector<Node*> left = _TransverseInorder(current->left);
    for(unsigned int i = 0; i < left.size(); i++){
        res.push_back(left[i]);
    }
    res.push_back(current);
    vector<Node*> right = _TransverseInorder(current->right);
    for(unsigned int i = 0; i < right.size(); i++){
        res.push_back(right[i]);
    }
    return res;
}

vector<Node*> BinaryTree::_TransversePreorder(Node* current){
    vector<Node*> res;
    if(current == nullptr)
        return res;
    res.push_back(current);
    vector<Node*> left = _TransversePreorder(current->left);
    for(unsigned int i = 0; i < left.size(); i++){
        res.push_back(left[i]);
    }
    vector<Node*> right = _TransversePreorder(current->right);
    for(unsigned int i = 0; i < right.size(); i++){
        res.push_back(right[i]);
    }
    return res;
}

vector<Node*> BinaryTree::_TransversePostorder(Node* current){
    vector<Node*> res;
    if(current == nullptr)
        return res;
    vector<Node*> left = _TransversePostorder(current->left);
    for(unsigned int i = 0; i < left.size(); i++){
        res.push_back(left[i]);
    }
    vector<Node*> right = _TransversePostorder(current->right);
    for(unsigned int i = 0; i < right.size(); i++){
        res.push_back(right[i]);
    }
    res.push_back(current);
    return res;
}

vector<Node*> BinaryTree::_TransverseDepthorder(Node* current){
    vector<Node*> res;
    vector<Node*> level_nodes;
    int level = 0;
    do{
        level_nodes = _GetTreeLevel(_root, level);
        for(unsigned int i = 0; i < level_nodes.size(); i++){
            res.push_back(level_nodes[i]);
        }
        level++;
    }while(level_nodes.size() > 0);
    return res;
}

vector<Node*> BinaryTree::_GetTreeLevel(Node* current, int level){
    vector<Node*> res;
    if(current == nullptr) { return res; }
    if(level == 0){
        res.push_back(current);
        return res;
    }

    vector<Node*> left = _GetTreeLevel(current->left, level - 1);
    for(unsigned int i = 0; i < left.size(); i++){
        res.push_back(left[i]);
    }
    vector<Node*> right = _GetTreeLevel(current->right, level - 1);
    for(unsigned int i = 0; i < right.size(); i++){
        res.push_back(right[i]);
    }
    return res;
}

vector<string> BinaryTree::TraverseValueInorder(){
    vector<Node*> nodes = _TransverseInorder(_root);
    vector<string> values;
    for(unsigned int i = 0; i < nodes.size(); i++){
        values.push_back(nodes[i]->value);
    }
    return values;
}
vector<string> BinaryTree::TraverseValuePreorder(){
    vector<Node*> nodes = _TransversePreorder(_root);
    vector<string> values;
    for(unsigned int i = 0; i < nodes.size(); i++){
        values.push_back(nodes[i]->value);
    }
    return values;
}
vector<string> BinaryTree::TraverseValuePostorder(){
    vector<Node*> nodes = _TransversePostorder(_root);
    vector<string> values;
    for(unsigned int i = 0; i < nodes.size(); i++){
        values.push_back(nodes[i]->value);
    }
    return values;
}
vector<string> BinaryTree::TraverseValueDepthorder(){
    vector<Node*> nodes = _TransverseDepthorder(_root);
    vector<string> values;
    for(unsigned int i = 0; i < nodes.size(); i++){
        values.push_back(nodes[i]->value);
    }
    return values;
}

vector<int> BinaryTree::TraverseIdInorder(){
    vector<Node*> nodes = _TransverseInorder(_root);
    vector<int> ids;
    for(unsigned int i = 0; i < nodes.size(); i++){
        ids.push_back(nodes[i]->id);
    }
    return ids;
}
vector<int> BinaryTree::TraverseIdPreorder(){
    vector<Node*> nodes = _TransversePreorder(_root);
    vector<int> ids;
    for(unsigned int i = 0; i < nodes.size(); i++){
        ids.push_back(nodes[i]->id);
    }
    return ids;
}
vector<int> BinaryTree::TraverseIdPostorder(){
    vector<Node*> nodes = _TransversePostorder(_root);
    vector<int> ids;
    for(unsigned int i = 0; i < nodes.size(); i++){
        ids.push_back(nodes[i]->id);
    }
    return ids;
}
vector<int> BinaryTree::TraverseIdDepthorder(){
    vector<Node*> nodes = _TransverseDepthorder(_root);
    vector<int> ids;
    for(unsigned int i = 0; i < nodes.size(); i++){
        ids.push_back(nodes[i]->id);
    }
    return ids;
}
