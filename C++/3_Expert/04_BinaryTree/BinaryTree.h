#ifndef Q4_H
#define Q4_H

#include <vector>
#include <string>

using namespace std;

// You may use this class to represent nodes in the tree. You may modify this class at your descression.
class Node {
    public:
        Node(int id, string value);
        ~Node();
        int id;
        string value;
        Node* left = nullptr;
        Node* right = nullptr;
        Node* parent = nullptr;
};

/*
API: 
 Implement the following Binary Tree class
 A binary tree is made up of nodes which contain some values. In this case an id and a value.
 Each node can have two child nodes (left and right). This tree is organised such that the id of the left node is less than the id of the parent node, and the id of the right node is greater than the id of the parent node.
 The first node is the root of the tree. All other nodes will be created as the child under this root node.
 In this case, this tree is used to store and retrieve data.
 The Add method is used to insert a string value with a unique int id. The id is used in the organisation of the tree. Each node store the id and the string value.
     So if first "foo" is inserted with id 10, then "bar" is inserted with id 9, a node with value 'foo' and id 10 is the root, and it's left node is a node is one with id 9 and value 'bar'
     If the value already exist do not insert it, and return false
 The Get method is used to retrieve a stored value. The unique id is provided, which can be used to traverse the tree efficiently, and retrieve it's value.
     If the id doesn't exist in the tree, return an empty string ""
 The Remove method is used to remove the node with the provided id. If the id is not in the tree, remove false
     When deleting a node, it's important to keep the integrity of the tree.
     If the deleted node is a leaf (has no children): remove the node
     If the deleted node has one child: the child takes it's place
     If the deleted node has two children: find the inorder successor node, copy the contents of that node to the deleted node, delete the successor node. Inorder traversal will be explained further down
 You also have to implement 4 methods of traversing the tree. When traversing the tree you visit every node given a certain algorithm. You have to return the values or the ids of each node, in the order you've visited them.
     Inorder traversal: you traverse the left subtree inorder, visit the root, then traverse the right subtree inorder
     Preorder traversal: you visit the root, traverse the left subtree preorder, then traverse the right subtree preorder
     Postorder traversal: you traverse the left subtree postorder, traverse the right subtree postorder, then visit the root
     Level order traversal: a level is defined as distance from the root. Visit the root, then visit all nodes left to right on the first level, then the second level, etc

NOTE: it is allowed in general to have binary trees where the left and right nodes don't have to be less than/greater than their parents. If they are, it's also refered to as a Binary Search Tree. In this exercises you must use a Binary Search Tree.

Example 1:
 BinaryTree t;
 t.Add(10, "foo");
 t.Add(9, "bar");
 t.Add(15, "baz");
 t.Add(12, "qux");
 t.Add(20, "dex");
 The resulting tree will look like this:
         (10, "foo")
         //        \\
     (9, "bar")  (15, "baz")
                 //        \\
             (12, "qux") (20, "dex")
 t.Get(15) -- "baz"
 t.TraverseValueInorder()    -- "bar", "foo", "qux", "baz", "dex"
 t.TraverseValuePreorder()   -- "foo", "bar", "baz", "qux", "dex"
 t.TraverseValuePostorder()  -- "bar", "qux", "dex", "baz", "foo"
 t.TraverseValueDepthorder() -- "foo", "bar", "baz", "qux", "dex"
 t.TraverseIdInorder()       -- 9, 10, 12, 15, 20
 t.TraverseIdPreorder()      -- 10, 9, 15, 12, 20
 t.TraverseIdPostorder()     -- 9, 12, 20, 15, 10
 t.TraverseIdDepthorder()    -- 10, 9, 15, 12, 20

Example 2:
 BinaryTree t;
 t.Add(10, "foo");
 t.Add(10, "bar"); -- False
 t.Add(12, "bar");
 t.Add(6, "baz");
 t.Add(11, "qux");
 t.Remove(10);
 t.Remove(14);   -- false
 The resulting tree will look like this:
         (11, "qux")
        //         \\
     (6, "baz")  (12, "bar")
*/

// DO NOT MODIFY THE PUBLIC MEMBERS THIS CLASS!
// THIS CLASS DEFINES THE STRUCTURE OF WHAT WILL BE TESTED
// IF YOU MODIFY THE PUBLIC STRUCUTRE OF THE CLASS, YOUR RESULTS WILL NOT BE VALID
class BinaryTree {
    public:
        BinaryTree();
        ~BinaryTree();
        bool Add(int id, string value);
        string Get(int id);
        bool Remove(int id);

        vector<string> TraverseValueInorder();
        vector<string> TraverseValuePreorder();
        vector<string> TraverseValuePostorder();
        vector<string> TraverseValueDepthorder();

        vector<int> TraverseIdInorder();
        vector<int> TraverseIdPreorder();
        vector<int> TraverseIdPostorder();
        vector<int> TraverseIdDepthorder();
    private:
        Node* _root;
        bool _insert(Node* current, int id, string value);
        Node* _find(Node* current, int id);
        void _print(Node* current, int indent);
        vector<Node*> _TransverseInorder(Node* current);
        vector<Node*> _TransversePreorder(Node* current);
        vector<Node*> _TransversePostorder(Node* current);
        vector<Node*> _TransverseDepthorder(Node* current);
        vector<Node*> _GetTreeLevel(Node* current, int level);

};

#endif // Q4_H