#include "cpp_eval_util.h"
#include <string>
#include "BinaryTree.h"

using namespace std;

#define TEST_CASE_COUNT 17

//BinaryTree: Add one, Get one
string case1(){
    BinaryTree t;
    t.Add(0, "foo");
    return t.Get(0);
}
//BinaryTree: Add multiple, Get one
string case2(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    return t.Get(15);
}
//BinaryTree: Add, Get not added
string case3(){
    BinaryTree t;
    t.Add(0, "foo");
    return t.Get(1);
}
//BinaryTree: Get
string case4(){
    BinaryTree t;
    return t.Get(0);
}
//BinaryTree: Add duplicate id
string case5(){
    BinaryTree t;
    t.Add(0, "foo");
    bool r = t.Add(0, "bar");
    return r ? "true" : "false";
}
//BinaryTree: Add duplicate value
string case6(){
    BinaryTree t;
    t.Add(0, "foo");
    bool r = t.Add(1, "foo");
    return r ? "true" : "false";
}
//BinaryTree: Add multiple, Remove last added, Get removed
string case7(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Remove(12);
    return t.Get(12);
}
//BinaryTree: Remove
string case8(){
    BinaryTree t;
    bool r = t.Remove(0);
    return r ? "true" : "false";
}
//BinaryTree: Add, Remove not added
string case9(){
    BinaryTree t;
    t.Add(0, "foo");
    bool r = t.Remove(1);
    return r ? "true" : "false";
}
//BinaryTree: Add multiple, Remove first added, Get all
string case10(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Remove(10);
    int counter = 0;
    if(t.Get(10) == "") counter++;
    if(t.Get(9) == "bar") counter++;
    if(t.Get(15) == "baz") counter++;
    if(t.Get(12) == "qux") counter++;
    if(t.Get(20) == "dex") counter++;
    return to_string(counter);
}
//BinaryTree: Add multiple, Remove one, Get all
string case11(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Remove(20);
    int counter = 0;
    if(t.Get(10) == "foo") counter++;
    if(t.Get(9) == "bar") counter++;
    if(t.Get(15) == "baz") counter++;
    if(t.Get(12) == "qux") counter++;
    if(t.Get(20) == "") counter++;
    if(t.Get(25) == "box") counter++;
    return to_string(counter);
}
// BinaryTree: Add one, Remove one, Get removed
string case12(){
    BinaryTree t;
    t.Add(0, "foo");
    t.Remove(0);
    return t.Get(0);
}
//BinaryTree: Add multiple, Remove one, Get all
string case13(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Add(5, "pax");
    t.Remove(20);
    t.Remove(10);
    t.Remove(15);
    int counter = 0;
    if(t.Get(10) == "") counter++;
    if(t.Get(9) == "bar") counter++;
    if(t.Get(15) == "") counter++;
    if(t.Get(12) == "qux") counter++;
    if(t.Get(20) == "") counter++;
    if(t.Get(25) == "box") counter++;
    if(t.Get(5) == "pax") counter++;
    return to_string(counter);
}
//BinaryTree: Add, Remove, Add again
string case14(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "baz");
    t.Remove(9);
    t.Add(9, "bar");
    return t.Get(9);
}
//BinaryTree: Add multiple, Delete all
string case15(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Remove(20);
    t.Remove(15);
    t.Remove(10);
    t.Remove(12);
    t.Remove(9);
    return t.Get(10) + t.Get(9) + t.Get(15) + t.Get(12) + t.Get(20);
}
//BinaryTree: Add, Remove multiple times
string case16(){
    BinaryTree t;
    for(int i = 0; i < 1000000; i++){
        t.Add(10, "foo");
        t.Add(9, "bar");
        t.Add(15, "baz");
        t.Add(12, "qux");
        t.Add(20, "dex");
        t.Remove(20);
        t.Remove(15);
        t.Remove(10);
        t.Remove(12);
        t.Remove(9);
    }
    return t.Get(10) + t.Get(9) + t.Get(15) + t.Get(12) + t.Get(20);
}
//BinaryTree: Create, Delete multiple times
string case17(){
    for(int i = 0; i < 3000000; i++){
        BinaryTree* t = new BinaryTree();
        t->Add(10, "foo");
        t->Add(9, "bar");
        t->Add(15, "baz");
        delete t;
    }
    return "";
}

class DataStorageEvaluator : public Evaluator<string>{
  public:
    string test_names[TEST_CASE_COUNT] = {
        "BinaryTree: Add one, Get one",
        "BinaryTree: Add multiple, Get one",
        "BinaryTree: Add, Get not added",
        "BinaryTree: Get",
        "BinaryTree: Add duplicate id",

        "BinaryTree: Add duplicate value",
        "BinaryTree: Add multiple, Remove last added, Get removed",
        "BinaryTree: Remove",
        "BinaryTree: Add, Remove not added",
        "BinaryTree: Add multiple, Remove first added, Get all",

        "BinaryTree: Add multiple, Remove one, Get all",
        "BinaryTree: Add one, Remove one, Get removed",
        "BinaryTree: Add multiple, Remove multiple, Get all",
        "BinaryTree: Add, Remove, Add again",
        "BinaryTree: Add multiple, Delete all",

        "BinaryTree: Add, Remove multiple times",
        "BinaryTree: Create, Delete multiple times"
    };

    string expected[TEST_CASE_COUNT] = {
        "foo",
        "baz",
        "",
        "",
        "false",

        "true",
        "",
        "false",
        "false",
        "5",

        "6",
        "",
        "7",
        "bar",
        "",

        "",
        ""
    };

    string (*functptr[TEST_CASE_COUNT])() = { 
        case1, case2, case3, case4, case5,
        case6, case7, case8, case9, case10,
        case11, case12, case13, case14, case15,
        case16, case17
        } ;

    DataStorageEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      return i < TEST_CASE_COUNT ? test_names[i] : "";
    }

    string GetResult(int i){
      return (*functptr[i])();
    }

    float GetScore(int i, string result){
      return result == expected[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, string result, float score){
      return "Returned \"" + result + "\", expecting \"" + expected[i] + "\"" + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  DataStorageEvaluator evaluator(argc, argv);
  return evaluator.Run();
}