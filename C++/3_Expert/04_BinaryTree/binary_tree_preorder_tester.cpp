#include "cpp_eval_util.h"
#include <string>
#include "BinaryTree.h"

using namespace std;

#define TEST_CASE_COUNT 8

//BinaryTree: Add one, TraverseIdPreorder
string case1(){
    BinaryTree t;
    t.Add(10, "foo");
    return vector_to_string(t.TraverseIdPreorder());
}
//BinaryTree: Add one, TraverseValuePreorder
string case2(){
    BinaryTree t;
    t.Add(10, "foo");
    return vector_to_string(t.TraverseValuePreorder());
}
//BinaryTree: Add multiple, TraverseIdPreorder
string case3(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Add(5, "pax");
    return vector_to_string(t.TraverseIdPreorder());
}
//BinaryTree: Add multiple, TraverseIdPreorder
string case4(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Add(5, "pax");
    return vector_to_string(t.TraverseValuePreorder());
}
//BinaryTree: Add multiple, Remove one, TraverseIdPreorder
string case5(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Add(5, "pax");
    t.Remove(15);
    return vector_to_string(t.TraverseIdPreorder());
}
//BinaryTree: Add multiple, Remove one, TraverseValuePreorder
string case6(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Add(5, "pax");
    t.Remove(15);
    return vector_to_string(t.TraverseValuePreorder());
}
//BinaryTree: Add multiple, Remove one, TraverseIdPreorder
string case7(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Remove(20);
    t.Remove(15);
    t.Remove(10);
    t.Remove(12);
    t.Add(10, "foo");
    t.Add(5, "box");
    t.Add(17, "fox");
    t.Add(2, "rex");
    return vector_to_string(t.TraverseIdPreorder());
}
//BinaryTree: Add multiple, Remove one, TraverseValuePreorder
string case8(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Remove(20);
    t.Remove(15);
    t.Remove(10);
    t.Remove(12);
    t.Add(10, "foo");
    t.Add(5, "box");
    t.Add(17, "fox");
    t.Add(2, "rex");
    return vector_to_string(t.TraverseValuePreorder());
}



class PreorderEvaluator : public Evaluator<string>{
  public:
    string test_names[TEST_CASE_COUNT] = {
        "BinaryTree: Add one, TraverseIdPreorder",
        "BinaryTree: Add one, TraverseValuePreorder",
        "BinaryTree: Add multiple, TraverseIdPreorder",
        "BinaryTree: Add multiple, TraverseValuePreorder",
        "BinaryTree: Add multiple, Remove one, TraverseIdPreorder",
        "BinaryTree: Add multiple, Remove one, TraverseValuePreorder",
        "BinaryTree: Add multiple, Remove multiple, Add multiple, TraverseIdPreorder",
        "BinaryTree: Add multiple, Remove multiple, Add multiple, TraverseValuePreorder",
    };

    string expected[TEST_CASE_COUNT] = {
        "{10}",
        "{\"foo\"}",
        "{10,9,5,15,12,20,25}",
        "{\"foo\",\"bar\",\"pax\",\"baz\",\"qux\",\"dex\",\"box\"}",
        "{10,9,5,20,12,25}",
        "{\"foo\",\"bar\",\"pax\",\"dex\",\"qux\",\"box\"}",
        "{9,5,2,10,17}",
        "{\"bar\",\"box\",\"rex\",\"foo\",\"fox\"}"
    };

    string (*functptr[TEST_CASE_COUNT])() = { 
        case1, case2,
        case3, case4,
        case5, case6,
        case7, case8 };

    PreorderEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      return i < TEST_CASE_COUNT ? test_names[i] : "";
    }

    string GetResult(int i){
      return (*functptr[i])();
    }

    float GetScore(int i, string result){
      return result == expected[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, string result, float score){
      return "Returned " + result + ", expecting " + expected[i] + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  PreorderEvaluator evaluator(argc, argv);
  return evaluator.Run();
}