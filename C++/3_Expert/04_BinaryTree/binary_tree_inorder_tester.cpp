#include "cpp_eval_util.h"
#include <string>
#include "BinaryTree.h"

using namespace std;

#define TEST_CASE_COUNT 8

//BinaryTree: Add one, TraverseIdInorder
string case1(){
    BinaryTree t;
    t.Add(10, "foo");
    return vector_to_string(t.TraverseIdInorder());
}
//BinaryTree: Add one, TraverseValueInorder
string case2(){
    BinaryTree t;
    t.Add(10, "foo");
    return vector_to_string(t.TraverseValueInorder());
}
//BinaryTree: Add multiple, TraverseIdInorder
string case3(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Add(5, "pax");
    return vector_to_string(t.TraverseIdInorder());
}
//BinaryTree: Add multiple, TraverseIdInorder
string case4(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Add(5, "pax");
    return vector_to_string(t.TraverseValueInorder());
}
//BinaryTree: Add multiple, Remove one, TraverseIdInorder
string case5(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Add(5, "pax");
    t.Remove(15);
    return vector_to_string(t.TraverseIdInorder());
}
//BinaryTree: Add multiple, Remove one, TraverseValueInorder
string case6(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Add(5, "pax");
    t.Remove(15);
    return vector_to_string(t.TraverseValueInorder());
}
//BinaryTree: Add multiple, Remove one, TraverseIdInorder
string case7(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Remove(20);
    t.Remove(15);
    t.Remove(10);
    t.Remove(12);
    t.Add(10, "foo");
    t.Add(5, "box");
    t.Add(17, "fox");
    t.Add(2, "rex");
    return vector_to_string(t.TraverseIdInorder());
}
//BinaryTree: Add multiple, Remove one, TraverseValueInorder
string case8(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Remove(20);
    t.Remove(15);
    t.Remove(10);
    t.Remove(12);
    t.Add(10, "foo");
    t.Add(5, "box");
    t.Add(17, "fox");
    t.Add(2, "rex");
    return vector_to_string(t.TraverseValueInorder());
}



class InorderEvaluator : public Evaluator<string>{
  public:
    string test_names[TEST_CASE_COUNT] = {
        "BinaryTree: Add one, TraverseIdInorder",
        "BinaryTree: Add one, TraverseValueInorder",
        "BinaryTree: Add multiple, TraverseIdInorder",
        "BinaryTree: Add multiple, TraverseValueInorder",
        "BinaryTree: Add multiple, Remove one, TraverseIdInorder",
        "BinaryTree: Add multiple, Remove one, TraverseValueInorder",
        "BinaryTree: Add multiple, Remove multiple, Add multiple, TraverseIdInorder",
        "BinaryTree: Add multiple, Remove multiple, Add multiple, TraverseValueInorder",
    };

    string expected[TEST_CASE_COUNT] = {
        "{10}",
        "{\"foo\"}",
        "{5,9,10,12,15,20,25}",
        "{\"pax\",\"bar\",\"foo\",\"qux\",\"baz\",\"dex\",\"box\"}",
        "{5,9,10,12,20,25}",
        "{\"pax\",\"bar\",\"foo\",\"qux\",\"dex\",\"box\"}",
        "{2,5,9,10,17}",
        "{\"rex\",\"box\",\"bar\",\"foo\",\"fox\"}"
    };

    string (*functptr[TEST_CASE_COUNT])() = { 
        case1, case2,
        case3, case4,
        case5, case6,
        case7, case8 };

    InorderEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      return i < TEST_CASE_COUNT ? test_names[i] : "";
    }

    string GetResult(int i){
      return (*functptr[i])();
    }

    float GetScore(int i, string result){
      return result == expected[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, string result, float score){
      return "Returned " + result + ", expecting " + expected[i] + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  InorderEvaluator evaluator(argc, argv);
  return evaluator.Run();
}