#include "cpp_eval_util.h"
#include <string>
#include "BinaryTree.h"

using namespace std;

#define TEST_CASE_COUNT 8

//BinaryTree: Add one, TraverseIdPostorder
string case1(){
    BinaryTree t;
    t.Add(10, "foo");
    return vector_to_string(t.TraverseIdPostorder());
}
//BinaryTree: Add one, TraverseValuePostorder
string case2(){
    BinaryTree t;
    t.Add(10, "foo");
    return vector_to_string(t.TraverseValuePostorder());
}
//BinaryTree: Add multiple, TraverseIdPostorder
string case3(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Add(5, "pax");
    return vector_to_string(t.TraverseIdPostorder());
}
//BinaryTree: Add multiple, TraverseIdPostorder
string case4(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Add(5, "pax");
    return vector_to_string(t.TraverseValuePostorder());
}
//BinaryTree: Add multiple, Remove one, TraverseIdPostorder
string case5(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Add(5, "pax");
    t.Remove(15);
    return vector_to_string(t.TraverseIdPostorder());
}
//BinaryTree: Add multiple, Remove one, TraverseValuePostorder
string case6(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Add(5, "pax");
    t.Remove(15);
    return vector_to_string(t.TraverseValuePostorder());
}
//BinaryTree: Add multiple, Remove one, TraverseIdPostorder
string case7(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Remove(20);
    t.Remove(15);
    t.Remove(10);
    t.Remove(12);
    t.Add(10, "foo");
    t.Add(5, "box");
    t.Add(17, "fox");
    t.Add(2, "rex");
    return vector_to_string(t.TraverseIdPostorder());
}
//BinaryTree: Add multiple, Remove one, TraverseValuePostorder
string case8(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Remove(20);
    t.Remove(15);
    t.Remove(10);
    t.Remove(12);
    t.Add(10, "foo");
    t.Add(5, "box");
    t.Add(17, "fox");
    t.Add(2, "rex");
    return vector_to_string(t.TraverseValuePostorder());
}



class PostorderEvaluator : public Evaluator<string>{
  public:
    string test_names[TEST_CASE_COUNT] = {
        "BinaryTree: Add one, TraverseIdPostorder",
        "BinaryTree: Add one, TraverseValuePostorder",
        "BinaryTree: Add multiple, TraverseIdPostorder",
        "BinaryTree: Add multiple, TraverseValuePostorder",
        "BinaryTree: Add multiple, Remove one, TraverseIdPostorder",
        "BinaryTree: Add multiple, Remove one, TraverseValuePostorder",
        "BinaryTree: Add multiple, Remove multiple, Add multiple, TraverseIdPostorder",
        "BinaryTree: Add multiple, Remove multiple, Add multiple, TraverseValuePostorder",
    };

    string expected[TEST_CASE_COUNT] = {
        "{10}",
        "{\"foo\"}",
        "{5,9,12,25,20,15,10}",
        "{\"pax\",\"bar\",\"qux\",\"box\",\"dex\",\"baz\",\"foo\"}",
        "{5,9,12,25,20,10}",
        "{\"pax\",\"bar\",\"qux\",\"box\",\"dex\",\"foo\"}",
        "{2,5,17,10,9}",
        "{\"rex\",\"box\",\"fox\",\"foo\",\"bar\"}"
    };

    string (*functptr[TEST_CASE_COUNT])() = { 
        case1, case2,
        case3, case4,
        case5, case6,
        case7, case8 };

    PostorderEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      return i < TEST_CASE_COUNT ? test_names[i] : "";
    }

    string GetResult(int i){
      return (*functptr[i])();
    }

    float GetScore(int i, string result){
      return result == expected[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, string result, float score){
      return "Returned " + result + ", expecting " + expected[i] + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  PostorderEvaluator evaluator(argc, argv);
  return evaluator.Run();
}