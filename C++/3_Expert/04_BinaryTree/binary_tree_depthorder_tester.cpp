#include "cpp_eval_util.h"
#include <string>
#include "BinaryTree.h"

using namespace std;

#define TEST_CASE_COUNT 8

//BinaryTree: Add one, TraverseIdDepthorder
string case1(){
    BinaryTree t;
    t.Add(10, "foo");
    return vector_to_string(t.TraverseIdDepthorder());
}
//BinaryTree: Add one, TraverseValueDepthorder
string case2(){
    BinaryTree t;
    t.Add(10, "foo");
    return vector_to_string(t.TraverseValueDepthorder());
}
//BinaryTree: Add multiple, TraverseIdDepthorder
string case3(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Add(5, "pax");
    return vector_to_string(t.TraverseIdDepthorder());
}
//BinaryTree: Add multiple, TraverseIdDepthorder
string case4(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Add(5, "pax");
    return vector_to_string(t.TraverseValueDepthorder());
}
//BinaryTree: Add multiple, Remove one, TraverseIdDepthorder
string case5(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Add(5, "pax");
    t.Remove(15);
    return vector_to_string(t.TraverseIdDepthorder());
}
//BinaryTree: Add multiple, Remove one, TraverseValueDepthorder
string case6(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Add(25, "box");
    t.Add(5, "pax");
    t.Remove(15);
    return vector_to_string(t.TraverseValueDepthorder());
}
//BinaryTree: Add multiple, Remove one, TraverseIdDepthorder
string case7(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Remove(20);
    t.Remove(15);
    t.Remove(10);
    t.Remove(12);
    t.Add(10, "foo");
    t.Add(5, "box");
    t.Add(17, "fox");
    t.Add(2, "rex");
    return vector_to_string(t.TraverseIdDepthorder());
}
//BinaryTree: Add multiple, Remove one, TraverseValueDepthorder
string case8(){
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");
    t.Remove(20);
    t.Remove(15);
    t.Remove(10);
    t.Remove(12);
    t.Add(10, "foo");
    t.Add(5, "box");
    t.Add(17, "fox");
    t.Add(2, "rex");
    return vector_to_string(t.TraverseValueDepthorder());
}



class DepthorderEvaluator : public Evaluator<string>{
  public:
    string test_names[TEST_CASE_COUNT] = {
        "BinaryTree: Add one, TraverseIdDepthorder",
        "BinaryTree: Add one, TraverseValueDepthorder",
        "BinaryTree: Add multiple, TraverseIdDepthorder",
        "BinaryTree: Add multiple, TraverseValueDepthorder",
        "BinaryTree: Add multiple, Remove one, TraverseIdDepthorder",
        "BinaryTree: Add multiple, Remove one, TraverseValueDepthorder",
        "BinaryTree: Add multiple, Remove multiple, Add multiple, TraverseIdDepthorder",
        "BinaryTree: Add multiple, Remove multiple, Add multiple, TraverseValueDepthorder",
    };

    string expected[TEST_CASE_COUNT] = {
        "{10}",
        "{\"foo\"}",
        "{10,9,15,5,12,20,25}",
        "{\"foo\",\"bar\",\"baz\",\"pax\",\"qux\",\"dex\",\"box\"}",
        "{10,9,20,5,12,25}",
        "{\"foo\",\"bar\",\"dex\",\"pax\",\"qux\",\"box\"}",
        "{9,5,10,2,17}",
        "{\"bar\",\"box\",\"foo\",\"rex\",\"fox\"}"
    };

    string (*functptr[TEST_CASE_COUNT])() = { 
        case1, case2,
        case3, case4,
        case5, case6,
        case7, case8 };

    DepthorderEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      return i < TEST_CASE_COUNT ? test_names[i] : "";
    }

    string GetResult(int i){
      return (*functptr[i])();
    }

    float GetScore(int i, string result){
      return result == expected[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, string result, float score){
      return "Returned " + result + ", expecting " + expected[i] + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  DepthorderEvaluator evaluator(argc, argv);
  return evaluator.Run();
}