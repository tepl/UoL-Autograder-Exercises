// YOU MAY MODIFY THIS FILE, AS LONG AS YOU DON'T MODIFY THE API DEFINED IN THE .H FILE
#include <iostream>
#include <cstdio>
#include <string.h>
#include "BinaryTree.h"

using namespace std;

void PrintArray(vector<int> arr){
    for(unsigned int i = 0; i < arr.size(); i++) {
        printf("\t%d\n", arr[i]);
    }
}

void PrintArray(vector<string> arr){
    for(unsigned int i = 0; i < arr.size(); i++) {
        printf("\t%s\n", arr[i].c_str());
    }
}

int main()
{
    BinaryTree t;
    t.Add(10, "foo");
    t.Add(9, "bar");
    t.Add(15, "baz");
    t.Add(12, "qux");
    t.Add(20, "dex");

    string r = t.Get(15);
    printf("Get(15): %s\n", r.c_str());

    vector<string> valueInorder = t.TraverseValueInorder();
    printf("Values inorder:\n");
    PrintArray(valueInorder);

    vector<string> valuePreorder = t.TraverseValuePreorder();
    printf("Values preorder:\n");
    PrintArray(valuePreorder);

    vector<string> valuePostorder = t.TraverseValuePostorder();
    printf("Values postorder:\n");
    PrintArray(valuePostorder);

    vector<string> valueDepthorder = t.TraverseValueDepthorder();
    printf("Values depthorder:\n");
    PrintArray(valueDepthorder);

    vector<int> idInorder = t.TraverseIdInorder();
    printf("Id inorder:\n");
    PrintArray(idInorder);
    
    vector<int> idPreorder = t.TraverseIdPreorder();
    printf("Id preorder:\n");
    PrintArray(idPreorder);

    vector<int> idPostorder = t.TraverseIdPostorder();
    printf("Id postorder:\n");
    PrintArray(idPostorder);

    vector<int> idDepthorder = t.TraverseIdDepthorder();
    printf("Id depthorder:\n");
    PrintArray(idDepthorder);
}