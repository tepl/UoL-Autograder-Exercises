#include "BinaryTree.h"


Node::Node(int id, string value) : value(value), id(id) {

}

Node::~Node(){
    
}

BinaryTree::BinaryTree() : _root(nullptr) {
}

BinaryTree::~BinaryTree(){
    
}

bool BinaryTree::Add(int id, string value){
    
}

bool BinaryTree::_insert(Node* current, int id, string value){
    
}

Node* BinaryTree::_find(Node* current, int id){
    
}

void BinaryTree::_print(Node* current, int indent){
    
}

bool BinaryTree::Remove(int v){
    
}

string BinaryTree::Get(int v){
    
}

vector<Node*> BinaryTree::_TransverseInorder(Node* current){
    
}

vector<Node*> BinaryTree::_TransversePreorder(Node* current){
    
}

vector<Node*> BinaryTree::_TransversePostorder(Node* current){
    
}

vector<Node*> BinaryTree::_TransverseDepthorder(Node* current){
    
}

vector<Node*> BinaryTree::_GetTreeLevel(Node* current, int level){
    
}

vector<string> BinaryTree::TraverseValueInorder(){
    
}

vector<string> BinaryTree::TraverseValuePreorder(){
    
}

vector<string> BinaryTree::TraverseValuePostorder(){
    
}

vector<string> BinaryTree::TraverseValueDepthorder(){
    
}

vector<int> BinaryTree::TraverseIdInorder(){
    
}

vector<int> BinaryTree::TraverseIdPreorder(){
    
}

vector<int> BinaryTree::TraverseIdPostorder(){
    
}

vector<int> BinaryTree::TraverseIdDepthorder(){
    
}
