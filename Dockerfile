FROM python:3.8.6

RUN apt-get update
RUN apt-get install -y cloc cppcheck clang-format
RUN pip install UoL-Autograder pytest
