import os
from shutil import copyfile
from zipfile import ZipFile
from tempfile import TemporaryDirectory
from . import enumerate_all_exercise_files, enumerate_all_tested_files
import argparse
from pathlib import Path

builder_parser = argparse.ArgumentParser(description="Build exercise files")
builder_parser.add_argument('--directory', "-d", type=str, default=".", help="Directory of files to be built")
builder_parser.add_argument('--generate', "-g", action="store_true", help="Generate empty files")
builder_parser.add_argument('--output_directory', '-o', type=str, default="./exercise_zips", help="Directory to store built files")

def generate_empty_files(starting_dir="."):
    for path, tested in enumerate_all_tested_files(starting_dir):
        empty_file_name = "_" + tested
        if not os.path.isfile(os.path.join(path, empty_file_name)):
            copyfile(os.path.join(path, tested), os.path.join(path, empty_file_name))

def zip_files(path, files, output_zip):
    tmp_dir = TemporaryDirectory()

    for f in files:
        copyfile(os.path.join(path, f), os.path.join(tmp_dir.name, f.lstrip("_")))
    
    with ZipFile(output_zip, "w") as zip_file:
        for f in os.listdir(tmp_dir.name):
            file_name = os.path.join(tmp_dir.name, f)
            if not os.path.isfile(file_name): continue
            zip_file.write(file_name, arcname=f)
    
    tmp_dir.cleanup()

def generate_zip_files(starting_dir, output_dir):
    for path, files in enumerate_all_exercise_files(starting_dir):
        if len(files) <= 0 or not any(f.startswith("_") for f in files):
            raise Exception(f"No empty file found in {path}. Empty files should prefixed with _")

        package_name = path.replace("\\", "/").replace("./", "").replace("/", "-")

        Path(output_dir).mkdir(parents=True, exist_ok=True)

        zip_files(path, files, os.path.join(output_dir, f"{package_name}.zip"))

def main():
    args = builder_parser.parse_args()

    if args.generate:
        generate_empty_files(args.directory)
    else:
        generate_zip_files(args.directory, args.output_directory)  
        

if __name__ == "__main__":
    main()
