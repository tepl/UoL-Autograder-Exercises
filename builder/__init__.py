import os

def select_tested(files):
    if len(h_files := [f for f in files if ".h" in f and not f.startswith("_")]) == 1 and h_files[0].replace(".h", ".cpp") in files:
        return h_files[0].replace(".h", ".cpp")
    elif len(cpp_files := [f for f in files if ".cpp" in f and not f.startswith("_")]) == 1:
        return cpp_files[0]
    elif len(py_files := [f for f in files if ".py" in f and "tester" not in f and not f.startswith("_")]) == 1:
        return py_files[0]
    else:
        raise Exception(f"Unable to find tested file within files: {files}")

def get_all_exercise_dirs(start_directory="."):
    for path, subdirs, files in os.walk(start_directory):
        if len(json_files := [f for f in files if ".json" in f and not f.startswith(".") and not "result" in f]) == 1 and len(files) > 1:
            yield path, files, json_files[0]

def enumerate_all_tested_files(start_directory="."):
    for path, files, config in get_all_exercise_dirs(start_directory):
        tested = select_tested(files)
        yield path, tested

def enumerate_all_exercises(start_directory="."):
    for path, files, config in get_all_exercise_dirs(start_directory):
        tested = select_tested(files)
        yield os.path.join(path, tested), os.path.join(path, config)

def enumerate_all_exercise_files(start_directory="."):
    for path, files, config in get_all_exercise_dirs(start_directory):
        tested = select_tested(files)

        exercise_files = []
        
        if ".cpp" in tested:
            h_file = tested.replace(".cpp", ".h")
            empty_h_file = "_" + h_file
            if os.path.isfile(os.path.join(path, empty_h_file)):
                exercise_files.append(empty_h_file)
            elif os.path.isfile(os.path.join(path, h_file)):
                exercise_files.append(h_file)
        
        if os.path.isfile(os.path.join(path, "main.cpp")):
            exercise_files.append("main.cpp")
        
        empty_file = "_" + tested

        if os.path.isfile(os.path.join(path, empty_file)):
            exercise_files.append(empty_file)
        
        yield path, exercise_files
