import pytest
import os
import subprocess
from builder import enumerate_all_exercises


@pytest.mark.parametrize(
    "tested, config",
    enumerate_all_exercises()
)
def test_all_exercises(tested, config):
    if os.path.isfile("result.json"):
        os.remove("result.json")
    subprocess.run(["feedback", tested, config, "-f", "result.json"])
    assert os.path.isfile("result.json")
