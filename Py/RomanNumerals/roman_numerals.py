from functools import reduce

# Given a whole number (int) return the same number converted into roman numerals.
# If the number is less than 1 or it's not an int, return an empty string ""
def int_to_roman_numeral(number):
    if type(number) != int or number <= 0:
        return ""

    dic = {1: 'I', 4: 'IV', 5: 'V', 9: 'IX', 10: 'X', 40: 'XL', 50: 'L',
           90: 'XC', 100: 'C', 400: 'CD', 500: 'D', 900: 'CM', 1000: 'M'}
    roman = ''
    for a in reversed(sorted(dic.keys())):
        while (a <= number):
            number = number - a
            roman = roman + dic[a]
    return roman

# Given a string representation of roman numberals, return the same number as a regular number
# If the string is not a roman number or it's not an int return 0
def roman_numeral_to_int(roman_numeral):
    if type(roman_numeral) != str or not all(c in "IVXLCDM" for c in roman_numeral) or len(roman_numeral) <= 0:
        return 0
    a = 0
    x = ['I', 'V', 'X', 'L', 'C', 'D', 'M']
    y = [1, 5, 10, 50, 100, 500, 1000]
    for i in range(len(roman_numeral)-1):
        if x.index(roman_numeral[i]) + 1 == x.index(roman_numeral[i+1]) or x.index(roman_numeral[i]) + 2 == x.index(roman_numeral[i+1]):
            a -= y[x.index(roman_numeral[i])]
        else:
            a += y[x.index(roman_numeral[i])]
    return a + y[x.index(roman_numeral[-1])]
