from py_eval_util import Evaluator

def test_file():

    test_cases = [
        (1, "I"), 
        (4, "IV"),
        (5, "V"),
        (6, "VI"), 
        (8, "VIII"),
        (9, "IX"),
        (10, "X"),
        (11, "XI"),
        (15, "XV"),
        (28, "XXVIII"),
        (48, "XLVIII"),
        (50, "L"),
        (54, "LIV"),
        (79, "LXXIX"),
        (93, "XCIII"),
        (99, "XCIX"),
        (100, "C"),
        (111, "CXI"),
        (386, "CCCLXXXVI"),
        (479, "CDLXXIX"),
        (508, "DVIII"),
        (632, "DCXXXII"),
        (765, "DCCLXV"),
        (999, "CMXCIX"),
        (1000, "M"),
        (1001, "MI"),
        (3200, "MMMCC"),
        (0, "not_roman_numerals"),
        (0, ""),
        (0, False),
        (0, 14)
    ]

    # Expected signature (the name and arguments will be matched with a function in the tested module)
    def roman_numeral_to_int(v):
        pass

    # Create evaluator object
    e = Evaluator()

    # For a given question i return the name of the question
    # This is used to discover the number of tests.
    # If i is greater than or equal to the number of tests, return None.
    # This indicates that the limit has been reached.
    e.with_name(lambda i: f"roman_numeral_to_int({test_cases[i][1]})" if i < len(test_cases) else None)

    # For a given question i, and a function from the module matching the requested signature, return some value that will be evaluated
    e.run_code(lambda i, f: f(test_cases[i][1]), signature=roman_numeral_to_int)

    # For a given question i which returned result lines, return a score that can be rewarded for it
    e.with_score(lambda i, result: 1 if result == test_cases[i][0] else 0 )
    
    # For a given question i which returned result lines and was awarded score return some appropriate feedback
    e.with_feedback(lambda i, result, score: f"Expected \"{test_cases[i][0]}\", recieved: \"{result}\" : " + ("PASS" if score >= 1 else "FAIL"))

    # Start evaluation
    e.start()


if __name__ == "__main__":
    test_file()
