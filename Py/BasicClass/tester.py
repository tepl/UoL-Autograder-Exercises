from py_eval_util import Evaluator

def test_file():

    test_cases = [
        "Foo",
        "Bar"
    ]

    # Expected signature (the name and arguments will be matched with a function in the tested module)
    class Foo:
        def __init__(self, param):
            pass

    def check_class(i, f):
        c = f(test_cases[i])
        if not hasattr(c, "get_bar"):
            return ""
        return c.get_bar()

    # Create evaluator object
    e = Evaluator()

    # For a given question i return the name of the question
    # This is used to discover the number of tests.
    # If i is greater than or equal to the number of tests, return None.
    # This indicates that the limit has been reached.
    e.with_name(lambda i: f"Foo: {test_cases[i]}" if i < len(test_cases) else None)  # Example only

    # For a given question i, and a function from the module matching the requested signature, return some value that will be evaluated
    e.run_code(check_class, signature=Foo)

    # For a given question i which returned result lines, return a score that can be rewarded for it
    e.with_score(lambda i, result: result == test_cases[i])
    
    # For a given question i which returned result lines and was awarded score return some appropriate feedback
    e.with_feedback(lambda i, result, score: f"Expected: {test_cases[i]}, Returned: {result} : " + ("PASS" if score >= 1 else "FAIL"))    # Example only

    # Start evaluation
    e.start()


if __name__ == "__main__":
    test_file()