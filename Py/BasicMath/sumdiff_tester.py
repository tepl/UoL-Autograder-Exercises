from py_eval_util import Evaluator

def test_file():
    test_cases = [
        ((1, 2), (3, -1)),
        ((3, 4), (7, -1)),
        ((5, 6), (11, -1)),
        ((7, 8), (15, -1)),
        ((9, 10), (19, -1))
    ]

    # Expected signature (the name and arguments will be matched with a function in the tested module)
    def sumdiff(x, y):
        pass

    e = Evaluator()
    e.with_name(
        lambda i: f"sumdiff{test_cases[i][0]}" if i < len(test_cases) else None)
    e.run_code(
        lambda i, f: f(*test_cases[i][0]), signature=sumdiff)
    e.with_score(
        lambda i, result: result == test_cases[i][1])
    e.with_feedback(
        lambda i, result, score: "Output: {}, Expected: {} : {}".format(result, test_cases[i][1], "PASS" if score >= 1 else "FAIL"))
    e.start()


if __name__ == "__main__":
    test_file()
