from py_eval_util import Evaluator
import random

def test_file():
    # Reference function, used for signiture and behaviour
    def fibonacci(n):
        if n<=1: 
            return 0
        elif n==2: 
            return 1
        else: 
            return fibonacci(n-1) + fibonacci(n-2)
    test_cases = random.sample(list(range(-5, 20)), 10)

    e = Evaluator()
    e.with_name(
        lambda i: f"fibonacci({test_cases[i]})" if i < len(test_cases) else None)
    e.run_code(
        lambda i, f: f(test_cases[i]), signature=fibonacci)
    e.with_score(
        lambda i, result: result == fibonacci(test_cases[i]))
    e.with_feedback(
        lambda i, result, score: "Output: {}, Expected: {} : {}".format(result, fibonacci(test_cases[i]), "PASS" if score >= 1 else "FAIL"))
    e.start()


if __name__ == "__main__":
    test_file()
