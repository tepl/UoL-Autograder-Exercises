from py_eval_util import Evaluator, numbers_close
from math import sqrt, sin, pow

def test_file():

    test_cases = [
        (1.2397, 7.2155, -4.7639, -2.6881),
        (2.8563, -3.6306, 5.3288, -0.3307),
        (-3.8322, 0.4902, -4.3235, 3.2525),
        (-0.0361, 6.5533, -1.8817, 7.5650),
        (0.9376, 4.5273, 10.5676, 1.6138),
    ]
    test_funcs = [
        (lambda a, b, c, d: max(a, b)),
        (lambda a, b, c, d: "NaN" if a <= 0 else pow(a, c)),
        (lambda a, b, c, d: "NaN" if d < 0 else sqrt(d)),
        (lambda a, b, c, d: sin(b)),
    ]
    expected = [[str(f(*test_case)) for f in test_funcs] for test_case in test_cases]

    # Expected signature (the name and arguments will be matched with a function in the tested module)
    def roman_numeral_to_int(v):
        pass

    # Create evaluator object
    e = Evaluator()

    # For a given question i return the name of the question
    # This is used to discover the number of tests.
    # If i is greater than or equal to the number of tests, return None.
    # This indicates that the limit has been reached.
    e.with_name(lambda i: f"input_math: {test_cases[i]}" if i < len(test_cases) else None)

    # Run the tested module as main
    e.run_module()

    e.with_input(lambda i: test_cases[i])

    # For a given question i which returned result lines, return a score that can be rewarded for it
    e.with_score(lambda i, result: all([o == e if e == 'NaN' else numbers_close(float(o), float(e), 1e-3) for o, e in zip(result, expected[i])]) )
    
    # For a given question i which returned result lines and was awarded score return some appropriate feedback
    e.with_feedback(lambda i, result, score: f"Expected {expected[i]}, recieved: {result} : " + ("PASS" if score >= 1 else "FAIL"))

    # Start evaluation
    e.start()


if __name__ == "__main__":
    test_file()
