import math

# Take 4 numbers passed in by the user, a, b, c and d
# Print to the console on separate lines the value of:
# higher value out of a and b
# a ^ c - if a <= 0 the string "NaN"
# the square root of d - if d < 0 the string "NaN"
# sin(b)
def main():
    a = float(input())
    b = float(input())
    c = float(input())
    d = float(input())

    print(max(a, b))
    print(math.pow(a, c) if a > 0 else "NaN")
    print(math.sqrt(d) if d >= 0 else "NaN")
    print(math.sin(b))


if __name__ == "__main__":
    main()