from py_eval_util import Evaluator, levenshtein_ratio


def test_file():
    test_cases = [
        "foo",
        "bar",
        "Hello World",
        "a" * 254
    ]

    def combined_provider(i, result):
        score = 0
        if len(consumed_input) == 0:
            feedback = "You didn't read in anything"
        elif len(result) == 0:
            if consumed_input[0] == test_cases[i]:
                feedback = "You've read the input, however failed to print anything"
                score = 0.2
            else:
                feedback = "Nothing was printed"
        elif consumed_input.too_many_reads:
            feedback = "You've tried to get inputs too many times"
        else:
            line = result[0]
            score = levenshtein_ratio(line, test_cases[i])
            if score >= 1:
                feedback = f"You've printed '{test_cases[i]}' correctly"
            else:
                feedback = f"Expected: '{test_cases[i]}' printed: '{line}'"

            if len(result) > 1:
                score -= 0.5
                feedback += ". You've printed more than one thing."
            
        feedback += " : PASS!" if score >= 1 else " : FAIL!"
        return score, feedback

    e = Evaluator()
    e.with_name(
        lambda i: f"Basic input: {test_cases[i]}" if i < len(test_cases) else None)
    e.run_module()
    e.with_input(
        lambda i: test_cases[i])
    e.with_score_and_feedback(combined_provider)
    e.start()

if __name__ == "__main__":
    test_file()
